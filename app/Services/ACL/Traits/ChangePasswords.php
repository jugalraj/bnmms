<?php

namespace App\Services\ACL\Traits;

use App\Http\Requests\ACL\User\ChangePasswordRequest;

/**
 * Class ChangePasswords
 * @package App\Services\ACL\Traits
 */
trait ChangePasswords
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangePasswordForm()
    {
        return view('auth.passwords.change');
    }

    /**
     * @param ChangePasswordRequest $request
     * @return mixed
     */
    public function changePassword(ChangePasswordRequest $request) {
        $this->user->changePassword($request->all());
        return redirect()->route('dashboard')->withFlashSuccess(trans('strings.dashboard.user.password_updated'));
    }
}
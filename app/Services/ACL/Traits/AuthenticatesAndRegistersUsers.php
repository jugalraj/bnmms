<?php

namespace App\Services\ACL\Traits;

/**
 * Class AuthenticatesAndRegistersUsers
 * @package App\Services\ACL\Traits
 */
trait AuthenticatesAndRegistersUsers
{
    use AuthenticatesUsers, RegistersUsers {
        AuthenticatesUsers::redirectPath insteadof RegistersUsers;
    }
}
<?php

namespace App\Models\Dashboard;

use Illuminate\Database\Eloquent\Model;

class Abode extends Model
{
    /**
     * Function : getAdobes
     * Function for retriving all addresses
     */
    public function getAdobes()
    {
        return $this->lists('district');
    }
}

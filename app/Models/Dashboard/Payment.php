<?php

namespace App\Models\Dashboard;

use Illuminate\Database\Eloquent\Model;
use App\Models\Dashboard\Traits\PaymentAttribute;

class Payment extends Model
{
    protected $table = 'member_payments';

    protected $fillable = [
      'payment_date', 'receipt_id','payment_modes','payment_amount', 'purpose','received_by', 'member_id', 'comments'
    ];

    /**
     * multiple record is owned by one member
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}

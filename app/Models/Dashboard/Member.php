<?php
namespace App\Models\Dashboard;

use App\Http\Requests\MembersRequest;
use App\Http\Requests\Request;
use App\Models\Dashboard\Traits\MemberAttribute;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed membership
 * @property mixed membership_type
 */
class Member extends Model
{
    use MemberAttribute;

    use SoftDeletes;

    protected $table = 'members';

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'registration_id',
        'membership_type',
        'first_name',
        'middle_name',
        'last_name',
        'date_of_birth',
        'gender',
        'perm_district',
        'perm_vdc',
        'perm_ward',
        'curr_vdc',
        'curr_district',
        'curr_ward',
        'tole',
        'contact_no',
        'mobile_no',
        'email',
        'profession',
        'state',
        'zip_or_postal',
        'city',
        'house_number',
        'street',
        'curr_country',
        'photo_path'
    ];

    protected $full_name ;

    /**
     * Member may have multiple records
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function membership()
    {
        return $this->hasOne(Membership::class);
    }

    public function getFullName()
    {
        return ucfirst($this->first_name).' '.ucfirst($this->middle_name).' '.ucfirst($this->last_name);
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return (new Carbon($this->date_of_birth))->diffInYears();
    }

    public function getAssociation()
    {
        if($this->membership_type == 'A')
            return 'Active Member';
        elseif ($this->membership_type == 'G')
            return 'General Member';
        elseif ($this->membership_type == 'V')
            return 'Volunteer';
        elseif ($this->membership_type == 'T')
            return 'Visitor';
        else
            return 'Friend';
    }

    public function getRegistrationDate()
    {
        if($this->membership)
            return $this->membership->registration_date;

        return null;
    }

    public function getMembershipExpiryDate()
    {
        if($this->membership)
            return $this->membership->renewal_date;

        return null;
    }

    public function getRegistrationID()
    {
        return ucfirst($this->first_name).$this->registration_id;
    }

    public function register($request)
    {
        $member_id = $this->getMembersID();

        $this->makeMembershipPayment($request,$member_id);

        Membership::create($request->all());
    }

    /**
     * @param $request
     * @param $member_id
     */
    private function makeMembershipPayment($request, $member_id)
    {
        Payment::makeMembershipPayment($request, $member_id);
    }

    private function getMembersID()
    {
        return $this->id;
    }
}

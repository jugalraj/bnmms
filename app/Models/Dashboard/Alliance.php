<?php

namespace App\Models\Dashboard;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Dashboard\Traits\AllianceAttribute;

class Alliance extends Model
{
    use AllianceAttribute;

    use SoftDeletes;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attribute is for deleted record
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'post',
        'organization',
        'address',
        'email',
        'mobile',
        'phone_1',
        'phone_2',
        'reference',
        'website',
    ];
}

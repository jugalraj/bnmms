<?php

namespace App\Models\Dashboard\Traits;


trait PeopleAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (access()->allow('edit-peoples')) {
            return '<a href="' . route('dashboard.peoples.edit', $this->id) . '" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"> Edit</a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
            if (access()->allow('delete-peoples')) {
                return '<a href="' . route('dashboard.peoples.destroy', $this->id) . '" class="btn btn-xs btn-danger" data-method="delete" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"> Delete</a>';
            }

        return '';
    }

    public function getPaymentButtonAttribute()
    {
            if (access()->allow('view-payments')) {
                return '<a href="' . route('dashboard.member.payment.index', $this->id) . '" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.payment') . '"> Payments</a>';
            }

        return '';
    }

    public function getMemberButtonAttribute()
    {
        if (access()->allow('make-member')) {
            if(in_array($this->membership_type, array('V','T','F')))
                return '<a href="' . route('dashboard.peoples.make_member', $this->id) . '" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.make_member') . '"> Upgrade</a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->disabled_status) {
            case 0:
                if (access()->allow('reactivate-peoples')) {
                    return '<a href="' . route('dashboard.people.mark', [$this->id, 1]) . '" class="btn btn-xs btn-success"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.dashboard.peoples.activate') . '"></i></a> ';
                }

                break;

            case 1:
                if (access()->allow('deactivate-peoples')) {
                    return '<a href="' . route('dashboard.people.mark', [$this->id, 0]) . '" class="btn btn-xs btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.dashboard.peoples.deactivate') . '"></i></a> ';
                }

                break;

            default:
                return '';
            // No break
        }

        return '';
    }


    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return
            $this->getMemberButtonAttribute().' '
            . $this->getPaymentButtonAttribute().' '
            . $this->getEditButtonAttribute()
            . $this->getDeleteButtonAttribute();
    }
}
<?php

namespace App\Models\ACL\Permission;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PermissionDependency
 * @package App\Models\ACL\Permission
 */
class PermissionDependency extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function permission()
    {
        return $this->hasOne(config('acl.permission_model'), 'id', 'dependency_id');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('acl.permission_dependencies_table');
    }
}
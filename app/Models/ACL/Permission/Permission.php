<?php

namespace App\Models\ACL\Permission;

use Illuminate\Database\Eloquent\Model;
use App\Models\ACL\Permission\Traits\Attribute\PermissionAttribute;

/**
 * Class Permission
 * @package App\Models\ACL\Permission
 */
class Permission extends Model
{
    use PermissionAttribute;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('acl.role_model'), config('acl.permission_role_pivot_table'), 'permission_id', 'role_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(config('acl.group_model'), 'group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dependencies()
    {
        return $this->hasMany(config('acl.dependency_model'), 'permission_id', 'id');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('acl.permission_table');
    }
}
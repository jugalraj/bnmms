<?php

Route::get('/', 'HomeController@index')->name('admin.home');

Route::get('/admin/setting/config', 'JugalAdminController@config')->name('admin.setting.config');

Route::post('/admin/setting/store', 'JugalAdminController@store')->name('admin.setting.store');
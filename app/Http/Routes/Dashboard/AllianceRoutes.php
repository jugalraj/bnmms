<?php

Route::group(['middleware' => 'web'], function () {

    Route::get('alliances/deleted', 'AlliancesController@deleted')->name('dashboard.alliances.deleted');

    Route::get('alliances/{member}/restore', 'AlliancesController@restore')->name('dashboard.alliances.restore');

    Route::resource('alliances', 'AlliancesController');

    Route::get('alliances/{member}/delete', 'AlliancesController@delete')->name('dashboard.alliances.delete-permanently');

});

<?php

Route::group(['middleware' => 'web'], function () {

    Route::get('members/inactive', 'MembersController@expired')->name('dashboard.members.inactive');

    Route::get('members/disabled', 'MembersController@disabled')->name('dashboard.members.disabled');

    Route::get('members/deleted', 'MembersController@deleted')->name('dashboard.members.deleted');

    Route::get('members/{member}/restore', 'MembersController@restore')->name('dashboard.members.restore');

    Route::get('members/{member}/mark/{status}', 'MembersController@mark')->name('dashboard.member.mark')->where(['status' => '[0,1]']);

    Route::resource('members', 'MembersController');

    Route::get('members/{member}/delete', 'MembersController@delete')->name('dashboard.members.delete-permanently');

    Route::get('member/{member}/payment/create', 'MemberPaymentsController@create')->name('dashboard.member.payment.create');

    Route::get('member/{member}/payment', 'MemberPaymentsController@index')->name('dashboard.member.payment.index');

    Route::post('member/{member}/payment', 'MemberPaymentsController@store')->name('dashboard.member.payment.store');

    Route::delete('member/{member}/payment/{payment}', 'MemberPaymentsController@destroy')->name('dashboard.member.payment.destroy');

    Route::get('member/{member}/payment/{payment}/edit', 'MemberPaymentsController@edit')->name('dashboard.member.payment.edit');

    Route::put('member/{member}/payment/{payment}', 'MemberPaymentsController@update')->name('dashboard.member.payment.update');

    Route::put('members/{member}/update', 'MembersController@update')->name('dashboard.members.update');

    Route::get('district/{district}/vdcs.json',function($district) {
        $vdcs =\App\Models\Dashboard\Abode::where('district',$district)->lists('vdc');
        foreach($vdcs as $vdc) {
                echo '<option value="' . $vdc . '">' . $vdc . '</option>';
        }
    })->name('dashboard.abodes');
});

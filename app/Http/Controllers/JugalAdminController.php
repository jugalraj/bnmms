<?php

namespace App\Http\Controllers;

use App\Models\Dashboard\JugalAdmin;
use App\Repositories\Admin\IJugalAdminRepositoryContract;
use Illuminate\Http\Request;

class JugalAdminController extends Controller
{
    /**
     * @var IJugalAdminRepositoryContract
     */
    protected $jugalAdmin;

    public function __construct(IJugalAdminRepositoryContract $jugalAdmin)
    {
        $this->jugalAdmin = $jugalAdmin;
    }

    var $skin_array = [
        'White Skin' => 'skin-white',
        'Blue Skin' => 'skin-blue',
        'Blue Light Skin' => 'skin-blue-light',
        'Black Skin' => 'skin-black',
        'Black Light Skin' => 'skin-black-light',
        'Purple Skin' => 'skin-purple',
        'Purple Light Skin' => 'skin-purple-light',
        'Yellow Skin' => 'skin-yellow',
        'Yellow Light Skin' => 'skin-yellow-light',
        'Red Skin' => 'skin-red',
        'Red Light Skin' => 'skin-red-light',
        'Green Skin' => 'skin-green',
        'Green Light Skin' => 'skin-green-light',
    ];

    var $layout_array = [
        'Fixed Layout' => 'fixed',
        'Sidebar Collapse Layout' => 'sidebar-collapse',
        'Mini Sidebar Layout' => 'sidebar-mini'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function config()
    {
        $configs = $this->jugalAdmin->getAllConfigs();

        return view('pages.admin.config',[
            'configs' => $configs,
            'skins'  =>  $this->skin_array,
            'layouts'    => $this->layout_array
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $all = $request->all();
        foreach(['sidebar_search', 'show_messages', 'show_notifications', 'show_tasks', 'show_rightsidebar'] as $key) {
            if(!isset($all[$key])) {
                $all[$key] = 0;
            } else {
                $all[$key] = 1;
            }
        }
        foreach($all as $key => $value) {
            JugalAdmin::where('key', $key)->update(['value' => $value]);
        }

        return redirect()->route('admin.setting.config')
            ->withFlashSuccess("Settings Saved Successfully");
    }
}

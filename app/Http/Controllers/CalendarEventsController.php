<?php

namespace App\Http\Controllers;

use App\Models\Dashboard\CalendarEvent;
use App\Models\Dashboard\People;
use App\Repositories\Dashboard\CalendarEvent\ICalendarEventRepositoryContract;
use Calendar;
use Illuminate\Http\Request;

use App\Http\Requests;
use MaddHatter\LaravelFullcalendar\Event;

class CalendarEventsController extends Controller
{
    protected $calendarEvent;

    /**
     * CalendarEventController constructor.
     * @param ICalendarEventRepositoryContract $calendarEvent
     */
    function __construct(
        ICalendarEventRepositoryContract $calendarEvent
    )
    {
        $this->calendarEvent = $calendarEvent;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $calendar_events = CalendarEvent::where('disabled_status',1)->get();

        return view('pages.events.index', compact('calendar_events'));
    }

    /**
     * Show the form for creating a new event.
     *
     * @return Response
     */
    public function create()
    {
        $event = new CalendarEvent();

        $peoples = People::all();

        $submitText = 'Create';

        return view('pages.events.create',compact('event','submitText','peoples'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->calendarEvent->create($request->all());

        return redirect()->route('dashboard.events.index')->withFlashSuccess('Calendar Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $calendar_event = CalendarEvent::findOrFail($id);

        return view('pages.events.show', compact('calendar_event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $calendar_event = CalendarEvent::findOrFail($id);

        return view('pages.events.edit', compact('calendar_event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int    $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $calendar_event = CalendarEvent::findOrFail($id);

        $calendar_event->title            = $request->input("title");
        $calendar_event->start            = $request->input("start");
        $calendar_event->end              = $request->input("end");
        $calendar_event->is_all_day       = $request->input("is_all_day");
        $calendar_event->background_color = $request->input("background_color");

        $calendar_event->save();

        return redirect()->route('pages.events.index')->with('message', 'Item updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $calendar_event = CalendarEvent::findOrFail($id);

        $calendar_event->delete();

        return redirect()->route('dashboard.events.index')->with('message', 'Item deleted successfully.');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendar()
    {
        $databaseEvents = $this->calendarEvent->getAllEvents();

        $calendar = Calendar::addEvents($databaseEvents);

        $calendar_events = CalendarEvent::all();

        return view('pages.events.calendar', compact('calendar','calendar_events'));
    }

    /**
     * @param $id
     * @param $disabled_status
     * @return mixed
     * @throws GeneralException
     * @internal param MarkUserRequest $request
     */
    public function mark($id, $disabled_status)
    {
        $calendarEvent = CalendarEvent::find($id);

        $calendarEvent->disabled_status = $disabled_status;

        if ($calendarEvent->save()) {
            return redirect()->back();
        }

        throw new GeneralException(trans('exceptions.dashboard.events.mark_error'));

    }

    public function canceled()
    {
        $calendar_events = CalendarEvent::where('disabled_status',0)->get();

        return view('pages.events.canceled', compact('calendar_events'));
    }

    public function deleted()
    {
        $calendar_events = CalendarEvent::onlyTrashed()->get();

        return view('pages.events.deleted', compact('calendar_events'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @internal param RestoreUserRequest $request
     */
    public function restore($id)
    {
        $calendarEvent = CalendarEvent::withTrashed()->find($id);

        if ($calendarEvent->restore()) {
            return redirect()->back()->withFlashSuccess(trans('alerts.dashboard.events.restored'));
        }

        throw new GeneralException(trans('exceptions.dashboard.access.events.restore_error'));
    }
}

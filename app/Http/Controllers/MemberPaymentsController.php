<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentRequest;
use App\Http\Utilities\NepaliDate;
use App\Models\Dashboard\Member;
use App\Models\Dashboard\Membership;
use App\Models\Dashboard\Payment;
use App\Repositories\Dashboard\Payments\IPaymentRepositoryContract;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MemberPaymentsController extends Controller
{

    private $payment;

    /**
     * MemberPaymentsController constructor.
     * @param IPaymentRepositoryContract $payment
     */
    function __construct(
        IPaymentRepositoryContract $payment
    )
    {
        $this->payment = $payment;
    }

    /**
     * @param Member $member
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Member $member)
    {
        $payments = $this->payment->getAllPayments($member->id);

        return view('pages.payments.index', compact('member', 'payments'));
    }

    /**
     * @param Member $member
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Member $member)
    {
        $payment = new Payment();
        $nepDate = new NepaliDate();
        return view('pages.payments.create', compact('member', 'payment','nepDate'));
    }

    public function store(PaymentRequest $request, Member $member)
    {
        $this->payment->create($request, $member);

        return redirect()->route('dashboard.member.payment.index',$member->id);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Member $member
     */
    public function show()
    {

    }

    /**
     * @param Payment $payment
     * @param Member $member
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Member $member, Payment $payment)
    {
        $submitText = ' Update ';
        return view('pages.payments.edit', compact('payment', 'member','submitText'));
    }

    /**
     * @param Request $request
     * @param Member $member
     * @param Payment $payment
     * @return \Illuminate\Http\RedirectResponse
     * @internal param $id
     * @internal param Payment $payment
     */
    public function update(Request $request, Member $member, Payment $payment)
    {
        $payment->update($request->all());

        return redirect()->route('dashboard.member.payment.index',$member->id);
    }
}

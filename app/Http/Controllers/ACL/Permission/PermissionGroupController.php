<?php

namespace App\Http\Controllers\ACL\Permission;

use App\Http\Controllers\Controller;
use App\Http\Requests\ACL\Permission\Group\SortPermissionGroupRequest;
use App\Http\Requests\ACL\Permission\Group\EditPermissionGroupRequest;
use App\Http\Requests\ACL\Permission\Group\StorePermissionGroupRequest;
use App\Http\Requests\ACL\Permission\Group\CreatePermissionGroupRequest;
use App\Http\Requests\ACL\Permission\Group\DeletePermissionGroupRequest;
use App\Http\Requests\ACL\Permission\Group\UpdatePermissionGroupRequest;
use App\Repositories\ACL\Permission\Group\PermissionGroupRepositoryContract;

/**
 * Class PermissionGroupController
 * @package App\Http\Controllers\ACL
 */
class PermissionGroupController extends Controller
{
    /**
     * @var PermissionGroupRepositoryContract
     */
    protected $groups;

    /**
     * @param PermissionGroupRepositoryContract $groups
     */
    public function __construct(PermissionGroupRepositoryContract $groups)
    {
        $this->groups = $groups;
    }

    /**
     * @param  CreatePermissionGroupRequest $request
     * @return \Illuminate\View\View
     */
    public function create(CreatePermissionGroupRequest $request)
    {
        return view('dashboard.acl.permissions.groups.create');
    }

    /**
     * @param  StorePermissionGroupRequest $request
     * @return mixed
     */
    public function store(StorePermissionGroupRequest $request)
    {
        $this->groups->store($request->all());
        return redirect()->route('dashboard.permissions.index')
            ->withFlashSuccess(trans('alerts.dashboard.permissions.groups.created'));
    }

    /**
     * @param  $id
     * @param  EditPermissionGroupRequest $request
     * @return mixed
     */
    public function edit($id, EditPermissionGroupRequest $request)
    {
        return view('dashboard.permission-groups.edit')
            ->withGroup($this->groups->find($id));
    }

    /**
     * @param  $id
     * @param  UpdatePermissionGroupRequest $request
     * @return mixed
     */
    public function update($id, UpdatePermissionGroupRequest $request)
    {
        $this->groups->update($id, $request->all());

        return redirect()->route('dashboard.permissions.index')
            ->withFlashSuccess(trans('alerts.dashboard.permissions.groups.created'));
    }

    /**
     * @param  $id
     * @param  DeletePermissionGroupRequest $request
     * @return mixed
     */
    public function destroy($id, DeletePermissionGroupRequest $request)
    {
        $this->groups->destroy($id);

        return redirect()->route('dashboard.permissions.index')
            ->withFlashSuccess(trans('alerts.dashboard.permissions.groups.deleted'));
    }

    /**
     * @param  SortPermissionGroupRequest      $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSort(SortPermissionGroupRequest $request)
    {
        $this->groups->updateSort($request->get('data'));
        return response()->json(['status' => 'OK']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Dashboard\Event;
use Illuminate\Http\Request;

use App\Http\Requests;
use MaddHatter\LaravelFullcalendar\Calendar;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Event $e
     * @return \Illuminate\Http\Response
     */
    public function index(Event $e)
    {
        $event = Event::where('id',$e->id);

        return view('pages.events.index', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $submitText = ' Add ';

        $event = new Event();

        return view('pages.events.create', compact('event', 'submitText'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Event $e
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Display the specified resource.
     *
     * @param Event $e
     * @return \Illuminate\Http\Response
     */
    public function calendar(Event $e)
    {
        $event = Event::where('id',$e->id);

        $events = [];

        $events[] = \Calendar::event(
            'Event One', //events title
            false, //full day events?
            '2016-11-11T0800', //start time (you can also use Carbon instead of DateTime)
            '2016-11-12T0800', //end time (you can also use Carbon instead of DateTime)
            0 //optionally, you can specify an events ID
        );

        $events[] = \Calendar::event(
            "Valentine's Day", //events title
            true, //full day events?
            new \DateTime('2015-02-14'), //start time (you can also use Carbon instead of DateTime)
            new \DateTime('2015-02-14'), //end time (you can also use Carbon instead of DateTime)
            'stringEventId' //optionally, you can specify an events ID
        );

        $eloquentEvent = Event::first(); //EventModel implements MaddHatter\LaravelFullcalendar\Event
//        dd($eloquentEvent);
        $calendar = \Calendar::addEvents($events) //add an array with addEvents
        ->addEvent($eloquentEvent, [ //set custom color fo this events
            'color' => '#800',
        ])->setOptions([ //set fullcalendar options
            'firstDay' => 1
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            'viewRender' => 'function() {alert("Callbacks!");}'
        ]);

        return view('pages.events.calendar', compact('calendar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

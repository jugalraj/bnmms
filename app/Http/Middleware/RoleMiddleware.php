<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RoleMiddleware
{
    protected $auth;

    /**
     * Creates a new instance of the middleware.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param $role
     * @return mixed
     * @internal param $roles
     */
    public function handle($request, Closure $next,$role)
    {
        if($this->auth->guest() || !$this->auth->user()->hasRole($role))
        {
            abort(404);
        }
        return $next($request);
    }
}

<?php
/**
 * Routes accessible by guest users
 */
Route::group(['middleware' => 'web'], function() {

    require (__DIR__ . '/Routes/ACL/PublicRoutes.php');

    /**
     * Routes for Home Page, redirecting back to
     * login page is not logged in otherwise home page
     */
    Route::get('/', 'HomeController@index')->name('dashboard');

    Route::get('/email', 'EmailController@index')->name('emails');

    Route::get('/email/compose', 'EmailController@compose')->name('email.create');

});

/**
 * Routes for logged in users with prefix
 * Namespaces indicate folder structure
 * Admin middleware groups web, auth, and routeNeedsPermission
 */
Route::group(['prefix' => 'dashboard', 'middleware' => 'admin'], function () {
    /**
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    require (__DIR__ . '/Routes/Dashboard/DashboardRoutes.php');
    require (__DIR__ . '/Routes/Dashboard/AccessRoutes.php');
    require (__DIR__ . '/Routes/Dashboard/PeopleRoutes.php');
    require (__DIR__ . '/Routes/Dashboard/MemberRoutes.php');
    require (__DIR__ . '/Routes/Dashboard/AllianceRoutes.php');
    require (__DIR__ . '/Routes/Dashboard/EventRoutes.php');

    Route::group(['namespace' => 'Auth'], function () {

        /**
         * These routes require the user to be logged in
         */
        Route::group(['middleware' => 'auth'], function () {
            Route::get('logout', 'AuthController@logout')->name('auth.logout');

            // Change Password Routes
            Route::get('password/change', 'PasswordController@showChangePasswordForm')->name('auth.password.change');
            Route::post('password/change', 'PasswordController@changePassword')->name('auth.password.update');
        });
    });
});

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PeoplesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'first_name' => 'required'
            ,'last_name' => 'required'
            ,'date_of_birth' => 'required|date'
            ,'gender' => 'required'
            ,'perm_district' => 'required'
            ,'perm_vdc' => 'required'
            ,'perm_ward' => 'required'
            ,'curr_district' => 'required'
            ,'curr_vdc' => 'required'
            ,'curr_ward' => 'required'
            ,'contact_no' => 'required'
            ,'email' => 'required'
        ];
    }
}

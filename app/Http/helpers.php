<?php

use \App\Models\Dashboard\People;
use \App\Http\Utilities\NepaliDate;

function action_for($path, $type ) {
    $csrf = csrf_field();

    if(is_object($path)) {
        $action = $path->getTable();

        if(in_array($type,['PUT','PATCH','DELETE'])){
            $action.='/'.$path->getKey();
        }
    }
    else {
        $action = $path;
    }
    return <<<EOT
        <form method='POST' action='{$action}' class='pull-right'>
            <input type='hidden' name='_method' value='{$type}' />
            $csrf
            <button class='btn btn-danger' type='submit'>Delete</button>
        </form>
EOT;

}

function get_users_name()
{
    return Auth::user()->full_name;
}

function users_register_date()
{
    return date_format(Auth::user()->created_at,'Y-m-d');
}

function get_search_box($method, $action, $data=[])
{
    return <<<EOT
     <form method="$method" action="$action" accept-charset="UTF-8" class="navbar-form navbar-left">
         <div class="form-group">
             <span class="icon"><i class="fa fa-search"></i></span>
             <input id="searchBox" class="form-control" placeholder="Search..." name="search" type="search">
         </div>
         <input type="submit" hidden/>
     </form>

EOT;
}

function show_app_version()
{
    return config('mis.app_version');
}

function show_organization_name()
{
    return config('mis.org_name');
}

function show_organization_website()
{
    return config('mis.org_website');
}

function app_name()
{
    return \App\Models\Dashboard\JugalAdmin::getByKey('sitename');
}

function access()
{
    return app('acl');
}

function getAllPeople()
{
    $peoples = People::all();

    return $peoples;
}

function toNepaliDate($engDate, $format = 'yyyy-mm-dd')
{
    $engDate = explode("-",$engDate);

    $nepDate = new NepaliDate();

    return $nepDate->nep_date($nepDate->eng_to_nep($engDate[0],$engDate[1], $engDate[1]), $format);
}

function toEnglishDate($nepdate, $format = 'yyyy-mm-dd')
{
    $nepDate = explode("-",$nepdate);

    $engDate = new NepaliDate();

    return $engDate->eng_date($engDate->nep_to_eng($nepDate[0],$nepDate[1], $nepDate[1]), $format);
}
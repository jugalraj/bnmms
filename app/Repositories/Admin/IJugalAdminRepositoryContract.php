<?php
namespace App\Repositories\Admin;


interface IJugalAdminRepositoryContract
{
    /**
     * @param  $id
     * @return mixed
     */
    public function findOrThrowException($id);

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @return mixed
     */
    public function getAllConfigs($order_by = 'id', $sort = 'asc');

    /**
     * @param $input
     * @return mixed
     */
    public function createConfigs($input);
}
<?php

namespace App\Repositories\Dashboard\Payments;


interface IPaymentRepositoryContract
{
    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws GeneralException
     */
    public function findOrThrowException($id);

    /**
     * @param $per_page
     * @param int $status
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getPaymentsPaginated($per_page, $status =1 , $order_by = 'id', $sort = 'asc');


    /**
     * @param null $member_id
     * @param bool $latest
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getAllPayments($member_id = NULL, $latest = true, $order_by = 'id', $sort = 'asc');

    /**
     * @param $request
     * @param $people
     * @return mixed
     */
    public function create($request, $people);

    /**
     * @param $request
     */
    public function createFirstPayment($request);

}


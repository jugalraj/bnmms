<?php

namespace App\Repositories\Dashboard\Alliance;

use Illuminate\Http\Request;
use App\Http\Requests\AllianceRequest;
use App\Models\Dashboard\Alliance;

/**
 * Interface IAllianceRepositoryContract
 * @package App\Repositories\Alliance
 */
interface IAllianceRepositoryContract
{
    /**
     * @param  $id
     * @return mixed
     */
    public function findOrThrowException($id);

    /**
     * @param $per_page
     * @param int $status
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getAlliancePaginated($per_page, $status =1 , $order_by = 'id', $sort = 'asc');

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @return mixed
     */
    public function getAllAlliance($order_by = 'id', $sort = 'asc');

    /**
     * @param AllianceRequest $request
     * @return
     */
    public function create($request);
}
<?php

namespace app\Repositories\Dashboard\Alliance;


use App\Http\Requests\AllianceRequest;
use App\Models\Dashboard\Alliance;
use App\Exceptions\GeneralException;
use Illuminate\Http\Request;

/**
 * @property Alliance alliance
 */
class AllianceRepository implements IAllianceRepositoryContract
{
    /**
     * Alliance Object
     */
    private $alliance;

    /**
     * Function : __constructor
     * Function for Constructor
     * @param Alliance $alliance
     */
     public function __construct(
         Alliance $alliance
     )
     {
         $this->alliance = $alliance;
     }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        if (! is_null(Alliance::find($id))) {
            return Alliance::findorfail($id);
        }

        throw new GeneralException('Alliances not found');
    }

    /**
     * @param $per_page
     * @param int $status
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getAlliancePaginated($per_page, $status =1 , $order_by = 'id', $sort = 'asc')
    {
        return Alliance::where('id','!=','1')
            ->orderBy($order_by, $sort)
            ->paginate($per_page);
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @return mixed
     */
    public function getAllAlliance($order_by = 'id', $sort = 'asc')
    {
        return Alliance::orderBy($order_by, $sort)
            ->get();
    }

    public function create($input)
    {
        if($this->alliance->create($input))
        {
            return true;
        }

        throw new GeneralException(trans('exceptions.dashboard.alliances.create_error'));
    }

    public function update($id, $input)
    {
        $alliance = $this->findOrThrowException($id);

        if ($alliance->update($input)) {
            $alliance->save();

            return true;
        }

        throw new GeneralException(trans('exceptions.dashboard.alliances.update_error'));
    }
}
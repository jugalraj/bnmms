<?php
/**
 * Created by PhpStorm.
 * User: rajan
 * Date: 1/7/2017
 * Time: 10:11 PM
 */

namespace App\Repositories\Dashboard\CalendarEvent;

use App\Exceptions\GeneralException;
use App\Models\Dashboard\CalendarEvent;
use Carbon\Carbon;

class CalendarEventRepository implements ICalendarEventRepositoryContract
{

    protected $calendarEvent;

    /**
     * CalendarEventRepository constructor.
     * @param CalendarEvent $calendarEvent
     */
    public function __construct(
        CalendarEvent $calendarEvent
    )
    {
        $this->calendarEvent = $calendarEvent;
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        if (!is_null(CalendarEvent::find($id))) {
            return CalendarEvent::findorfail($id);
        }

        throw new GeneralException('Alliances not found');
    }

    /**
     * @param $per_page
     * @param int $status
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getEventPaginated($per_page, $status = 1, $order_by = 'id', $sort = 'asc')
    {
        return CalendarEvent::where('id', '!=', '1')
            ->orderBy($order_by, $sort)
            ->paginate($per_page);
    }

    /**
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getAllEvents($order_by = 'id', $sort = 'asc')
    {
        return CalendarEvent::orderBy($order_by, $sort)
            ->get();
    }

    public function create($input)
    {
        $event = new CalendarEvent;
        $event->title = $input['title'];
        $event->desc = $input['desc'];
        $event->start = Carbon::createFromFormat('Y-m-d h:i A',$input['start']);
        $event->end = Carbon::createFromFormat('Y-m-d h:i A',$input['start']);
        $event->background_color = $input['background_color'];
        $event->event_coordinator =  implode(',',$input['event_coordinator']);

        //Fill the five star of campaign
        if($input['event_type'] == 'Campaign') {
            $event->media_manager = implode(',', $input['media_manager']);
            $event->fund_raiser = implode(',', $input['fund_raiser']);
            $event->logistic_manager = implode(',', $input['logistic_manager']);
            $event->volunteer_manager = implode(',', $input['volunteer_manager']);
        }

        if($event->save())
            return true;

        throw new GeneralException(trans('exceptions.dashboard.events.create_error'));
    }

    public function getUpcomingEvents()
    {
        // TODO: Implement getUpcomingEvents() method.
    }

    public function getCanceledEvents()
    {
        // TODO: Implement getCanceledEvents() method.
    }

    public function getCompletedEvents()
    {
        // TODO: Implement getCompletedEvents() method.
    }
}
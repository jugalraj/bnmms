<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberships', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id');
            $table->date('registration_date');
            $table->date('renewal_date');
            $table->string('Membership_year');
            $table->string('membership_type',45);
            $table->string('verified_document');
            $table->string('document_number');
            $table->string('document_path');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Memberships');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionDependenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('acl.permission_dependencies_table'), function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('permission_id');
            $table->unsignedInteger('dependency_id');
            $table->timestamps();

            $table->foreign('permission_id')
                ->references('id')
                ->on(config('acl.permission_table'))
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('dependency_id')
                ->references('id')
                ->on(config('acl.permission_table'))
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(config('acl.permission_dependencies_table'));
    }
}

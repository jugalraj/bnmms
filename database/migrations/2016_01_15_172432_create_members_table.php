<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('registration_id')->unique();
            $table->string('membership_type', 2);
            $table->string('first_name', 45);
            $table->string('middle_name', 45);
            $table->string('last_name', 45);
            $table->string('gender',45);
            $table->date('date_of_birth')->nullable();
            $table->string('perm_district',40);
            $table->string('perm_vdc',40);
            $table->string('perm_ward',40);
            $table->string('curr_country');
            $table->string('curr_district',40)->nullable();
            $table->string('curr_vdc',40)->nullable();
            $table->string('curr_ward',40)->nullable();
            $table->string('tole',100)->nullable();
            $table->string('state',50)->nullable();
            $table->string('zip_or_postal',10)->nullable();
            $table->string('city',50)->nullable();
            $table->integer('house_number')->nullable();
            $table->string('street',45)->nullable();
            $table->string('contact_no')->length(20);
            $table->string('mobile_no')->length(20)->unique();
            $table->string('email',50)->unique();
            $table->string('profession',30)->nullable();
            $table->string('photo_path',250)->nullable();
            $table->boolean('disabled_status')->default(1);
            $table->boolean('membership_status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('members');
    }
}

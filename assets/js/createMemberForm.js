
$(document).ready(function(){
	
	$("#form-wizard").formwizard({
		formPluginEnabled: true,
		validationEnabled: true,
		focusFirstInput : true,
		disableUIStyles : true,
	
		formOptions :{
			success: function(data){$("#status").fadeTo(500,1,function(){ $(this).html("<span>Form was submitted!</span>").fadeTo(5000, 0); })},
			beforeSubmit: function(data){$("#submitted").html("<span>Form was submitted with ajax. Data sent to the server: " + $.param(data) + "</span>");},
			dataType: 'json',
			resetForm: true
		},
		validationOptions : {
			rules: {
				first_name: "required",
				last_name: "required",
				date_of_birth : "required",
				gender: "required",
				contact_no : "required",
				profession: "required",
				perm_district: "required",
				perm_vdc : "required",
				perm_ward : "required",
				curr_district: "required",
				curr_vdc : "required",
				curr_ward : "required",
				registration_date: "required",
				renewal_date: "required",
				membership_type: "required",
				verified_document: "required",
				document_number: "required",
				document_image: "required",
				payment_date: "required",
				receipt_id: "required",
				payment_modes: "required",
				payment_amount: "required"
			},
			messages: {
				first_name: "First Name Required",
				last_name: "Last Name Required",
				date_of_birth : "Date Of Birth Required",
				gender: "Gender Required",
				email: { required: "Email Required", email: "Correct email format is name@domain.com" },
				contact_no : "Phone No. Required",
				qualification: "Qualification Required",
				members_photo: "Members Photo Required",
				perm_district: "Permanent District Required",
				perm_vdc : "Permanent VDC Required",
				perm_ward : "Permanent Ward Required",
				curr_district: "Current District Required",
				curr_vdc : "Current VDC Required",
				curr_ward : "Current Ward Required",
				registration_date: "Registration Date Required",
				renewal_date: "Renewal Date Required",
				membership_type: "Membership Type Required",
				verified_document: "Verified Document Required",
				document_number: "Verified Document Number Required",
				document_image: "Image of Verified Document Required",
				payment_date: "Payment Date Required",
				receipt_id: "Receipt ID Required",
				payment_modes: "Payment Mode Required",
				payment_amount: "Payment Amount Required"
			},
			errorClass: "help-block",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.form-group').removeClass('has-error');
			}
		}
	});	
});

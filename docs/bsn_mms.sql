-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 29. Feb 2016 um 09:22
-- Server Version: 5.6.17
-- PHP-Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `bsn_mms`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `basicdata`
--

CREATE TABLE IF NOT EXISTS `basicdata` (
  `MemberID` int(11) NOT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `MiddleName` varchar(45) DEFAULT NULL,
  `Surname` varchar(45) DEFAULT NULL,
  `DateOfBirth` datetime DEFAULT NULL,
  `Gender` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`MemberID`),
  UNIQUE KEY `MemberID_UNIQUE` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `memberidcreation`
--

CREATE TABLE IF NOT EXISTS `memberidcreation` (
  `MemberID` int(11) NOT NULL DEFAULT '100',
  `MemberType` char(1) NOT NULL,
  PRIMARY KEY (`MemberID`),
  UNIQUE KEY `idBasicData_UNIQUE` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `membershipdata`
--

CREATE TABLE IF NOT EXISTS `membershipdata` (
  `MemberID` int(11) NOT NULL,
  `MembershipSince` datetime DEFAULT NULL,
  `MembershipType` varchar(45) DEFAULT NULL,
  `ValidUntil` datetime DEFAULT NULL,
  `VerifiedDocument` varchar(45) DEFAULT NULL,
  `DocumentNumber` varchar(45) DEFAULT NULL,
  `DocumentPhoto` blob,
  `MembershipTerminated` datetime DEFAULT NULL,
  `TerminationReason` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`MemberID`),
  UNIQUE KEY `MemberID_UNIQUE` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `paymentdata`
--

CREATE TABLE IF NOT EXISTS `paymentdata` (
  `PaymentDataID` int(11) NOT NULL AUTO_INCREMENT,
  `MemberID` int(11) NOT NULL,
  `PaymentDate` datetime DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  `Currency` varchar(45) DEFAULT NULL,
  `InNepaliRs` int(11) DEFAULT NULL,
  `Purpose` varchar(45) DEFAULT NULL,
  `PurposeDetail` varchar(500) DEFAULT NULL,
  `PaymentMedium` varchar(45) DEFAULT NULL,
  `ReceiptNumber` varchar(45) DEFAULT NULL,
  `ReceiptPhoto` blob,
  PRIMARY KEY (`PaymentDataID`),
  UNIQUE KEY `PaymentDataID_UNIQUE` (`PaymentDataID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `basicdata`
--
ALTER TABLE `basicdata`
  ADD CONSTRAINT `MemberID` FOREIGN KEY (`MemberID`) REFERENCES `memberidcreation` (`MemberID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

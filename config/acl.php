<?php

return [

    'user_model' => App\Models\ACL\User\User::class,

    /*
     * Role model used by ACL to create correct relations. Update the role if it is in a different namespace.
    */
    'role_model' => App\Models\ACL\Role\Role::class,

    /*
     * Roles table used by ACL to save roles to the database.
     */
    'role_table' => 'roles',

    /*
     * Permission model used by ACL to create correct relations.
     * Update the permission if it is in a different namespace.
     */
    'permission_model' => App\Models\ACL\Permission\Permission::class,

    /*
     * Permissions table used by ACL to save permissions to the database.
     */
    'permission_table' => 'permissions',

    /*
     * PermissionGroup model used by ACL to create permissions groups.
     * Update the group if it is in a different namespace.
     */
    'group_model' => App\Models\ACL\Permission\PermissionGroup::class,

    /*
     * Permissions table used by ACL to save permissions to the database.
     */
    'group_table' => 'groups',

    /*
     * PermissionDependency model used by ACL to create permissions dependencies.
     * Update the dependency if it is in a different namespace.
     */
    'dependency_model' => App\Models\ACL\Permission\PermissionDependency::class,

    /*
     * Table that specifies if one permission is dependent on another.
     * For example in order for a user to have the edit-user permission they also need the view-backend permission.
     */
    'permission_dependencies_table' => 'permission_dependencies',

    /*
     * permission_role table used by ACL to save relationship between permissions and roles to the database.
     */
    'permission_role_pivot_table' => 'permission_role',

    /*
     * permission_user table used by ACL to save relationship between permissions and users to the database.
     * This table is only for permissions that belong directly to a specific user and not a role
     */
    'permission_user_pivot_table' => 'permission_user',

    /*
     * assigned_roles table used by ACL to save assigned roles to the database.
     */
    'user_role_pivot_table' => 'role_user',

    /*
     * Configurations for the user
     */
    'users' => [
        /*
         * Administration tables
         */
        'default_per_page' => 25,

        /*
         * The role the user is assigned to when they sign up from the frontend, not namespaced
         */
        'default_role' => 'User',
        //'default_role' => 2,

        /*
         * Whether or not the user has to confirm their email when signing up
         */
        'confirm_email' => true,

        /*
         * Whether or not the users email can be changed on the edit profile screen
         */
        'change_email' => false,
    ],

    /*
     * Configuration for roles
     */
    'roles' => [
        /*
         * Whether a role must contain a permission or can be used standalone as a label
         */
        'role_must_contain_permission' => true
    ],

    /*
     * Socialite session variable name
     * Contains the name of the currently logged in provider in the users session
     * Makes it so social logins can not change passwords, etc.
     */
    'socialite_session_name' => 'socialite_provider',
];
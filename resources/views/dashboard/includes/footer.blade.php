<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Version 1.0
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {!! date('Y') !!} <a href="#">{!! app_name() !!}</a>.</strong> {{ trans('strings.dashboard.general.all_rights_reserved') }}
</footer>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('/assets/img/avatar.png') }}" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>{!! access()->user()->name !!}</p>
                <!-- Status -->
                <a href="#"><i
                            class="fa fa-circle text-success"></i> {{ trans('strings.dashboard.general.status.online') }}
                </a>
            </div>
        </div>

        <!-- search form (Optional) -->
    {{--<form action="#" method="get" class="sidebar-form">--}}
    {{--<div class="input-group">--}}
    {{--<input type="text" name="q" class="form-control"--}}
    {{--placeholder="{{ trans('strings.dashboard.general.search_placeholder') }}"/>--}}
    {{--<span class="input-group-btn">--}}
    {{--<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i--}}
    {{--class="fa fa-search"></i></button>--}}
    {{--</span>--}}
    {{--</div>--}}
    {{--</form>--}}
    <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.admin.sidebar.general') }}</li>

            <!-- Optionally, you can add icons to the links -->
            <li class="{{ Active::pattern('/') }}">
                <a href="{!! route('dashboard') !!}"><i
                            class="fa fa-dashboard"></i><span>{{ trans('menus.admin.sidebar.dashboard') }}</span></a>
            </li>

            @permission('view-access-management')
            <li class="{{ Active::pattern('dashboard*/users') }} treeview">
                <a href="#">
                    <i class="fa phpdebugbar-fa-users"></i>
                    <span>{{ trans('menus.admin.acl.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('dashboard/users*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('dashboard/users*', 'display: block;') }}">
                    {{--Users Sidebar Menu--}}
                    <li class="{{ Active::pattern('dashboard/users*') }} treeview">
                        <a href="#">
                            <span>{{ trans('menus.admin.acl.users.main') }}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu {{ Active::pattern('dashboard/users*', 'menu-open') }}"
                            style="display: none; {{ Active::pattern('dashboard/users*', 'display: block;') }}">
                            <li class="{{ Active::pattern('dashboard/users/create') }}">
                                <a href="{!! url('dashboard/users/create') !!}">
                                    <i class="fa fa-user-plus"></i>
                                    {{ trans('menus.admin.acl.users.create') }}
                                </a>
                            </li>
                            <li class="{{ Active::pattern('dashboard/users') }}">
                                <a href="{!! url('dashboard/users') !!}">
                                    <i class="fa fa-users"></i>
                                    {{ trans('menus.admin.acl.users.all') }}
                                </a>
                            </li>
                            <li class="{{ Active::pattern('dashboard/users/deactivated') }}">
                                <a href="{!! url('dashboard/users/deactivated') !!}">
                                    <i class="fa fa-user-secret"></i>
                                    {{ trans('menus.admin.acl.users.deactivated') }}
                                </a>

                            </li>
                            <li class="{{ Active::pattern('dashboard/users/deleted') }}">
                                <a href="{!! url('dashboard/users/deleted') !!}">
                                    <i class="fa fa-user-times"></i>
                                    {{ trans('menus.admin.acl.users.deleted') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{-- Roles Sidebar Menu --}}
                    <li class="{{ Active::pattern('dashboard/roles*') }} treeview">
                        <a href="#">
                            <span>{{ trans('menus.admin.acl.roles.main') }}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu {{ Active::pattern('dashboard/roles*', 'menu-open') }}"
                            style="display: none; {{ Active::pattern('dashboard/roles*', 'display: block;') }}">
                            <li class="{{ Active::pattern('dashboard/roles/create') }}">
                                <a href="{!! url('dashboard/roles/create') !!}">
                                    <i class="fa fa-circle-o"></i>
                                    {{ trans('menus.admin.acl.roles.create') }}
                                </a>
                            </li>
                            <li class="{{ Active::pattern('dashboard/roles') }}">
                                <a href="{!! url('dashboard/roles') !!}">
                                    <i class="fa fa-circle-o"></i>
                                    {{ trans('menus.admin.acl.roles.all') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{-- Permissions Sidebar Menu --}}
                    <li class="{{ Active::pattern('dashboard/permissions*') }} treeview">
                        <a href="#">
                            <span>{{ trans('menus.admin.acl.permissions.main') }}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu {{ Active::pattern('dashboard/permission*', 'menu-open') }}"
                            style="display: none; {{ Active::pattern('dashboard/permission*', 'display: block;') }}">
                            <li class="{{ Active::pattern('dashboard/permissions/create') }}">
                                <a href="{!! url('dashboard/permissions/create') !!}">
                                    <i class="fa fa-user-plus"></i>
                                    {{ trans('menus.admin.acl.permissions.create') }}
                                </a>
                            </li>
                            <li class="{{ Active::pattern('dashboard/permissions#all-permissions') }}">
                                <a href="{!! url('dashboard/permissions#all-permissions') !!}">
                                    <i class="fa fa-users"></i>
                                    {{ trans('menus.admin.acl.permissions.all') }}
                                </a>
                            </li>
                            <li class="{{ Active::pattern('dashboard/permission-group/create') }}">
                                <a href="{!! url('dashboard/permission-group/create') !!}">
                                    <i class="fa fa-user-secret"></i>
                                    {{ trans('menus.admin.acl.permissions.groups.create') }}
                                </a>

                            </li>
                            <li class="{{ Active::pattern('dashboard/permissions/groups') }}">
                                <a href="{!! url('dashboard/permissions') !!}">
                                    <i class="fa fa-user-times"></i>
                                    {{ trans('menus.admin.acl.permissions.groups.main') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            @end

            @permission('manage-site-setting')
            <li class="{{ Active::pattern('dashboard/admin/setting/*') }} treeview">
                <a href="#">
                    <i class="fa fa-wrench"></i>
                    <span>{{ trans('menus.admin.setting.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('dashboard/admin/setting*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('dashboard/admin/setting/*', 'display: block;') }}">
                    @permission('manage-config')
                    <li class="{{ Active::pattern('setting') }}">
                        <a href="{!! route('admin.setting.config') !!}">
                            <i class="fa fa-gear"></i>
                            {{ trans('menus.admin.setting.config') }}
                        </a>
                    </li>
                    @end
                </ul>
            </li>
            @end


            @permission('manage-peoples')
            <li class="{{ Active::pattern('dashboard/peoples/*') }} treeview">
                <a href="#">
                    <i class="fa fa-male"></i>
                    <span>{{ trans('menus.admin.people.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('dashboard/peoples*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('dashboard/peoples*', 'display: block;') }}">
                    @permission('create-peoples')
                    <li class="{{ Active::pattern('dashboard/peoples/create') }}">
                        <a href="{!! route('dashboard.peoples.create') !!}">
                            <i class="fa fa-user-plus"></i>
                            {{ trans('menus.admin.people.add') }}
                        </a>
                    </li>
                    @end
                    <li class="{{ Active::pattern('dashboard/peoples') }}">
                        <a href="{!! route('dashboard.peoples.index') !!}">
                            <i class="fa fa-user"></i>
                            {{ trans('menus.admin.people.view') }}
                        </a>
                    </li>
                </ul>
            </li>
            @end

            @permission('manage-members')
            <li class="{{ Active::pattern('dashboard/members/*') }} treeview">
                <a href="#">
                    <i class="fa fa-magnet"></i>
                    <span>{{ trans('menus.admin.member.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('dashboard/members*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('dashboard/members*', 'display: block;') }}">
                    @permission('create-members')
                    <li class="{{ Active::pattern('dashboard/members/create') }}">
                        <a href="{!! route('dashboard.members.create') !!}">
                            <i class="fa fa-user-plus"></i>
                            {{ trans('menus.admin.member.add') }}
                        </a>
                    </li>
                    @end
                    <li class="{{ Active::pattern('dashboard/members') }}">
                        <a href="{!! route('dashboard.members.index') !!}">
                            <i class="fa fa-user"></i>
                            {{ trans('menus.admin.member.view') }}
                        </a>
                    </li>
                    <li class="{{ Active::pattern('dashboard/members/disabled') }}">
                        <a href="{!! url('dashboard/members/disabled') !!}">
                            <i class="fa fa-user-md"></i>
                            {{ trans('menus.admin.member.disabled') }}
                        </a>
                    </li>
                    <li class="{{ Active::pattern('dashboard/members/inactive') }}">
                        <a href="{!! url('dashboard/members/inactive') !!}">
                            <i class="fa fa-user-md"></i>
                            {{ trans('menus.admin.member.inactive') }}
                        </a>
                    </li>
                    @permission('restore-members')
                    <li class="{{ Active::pattern('dashboard/members/deleted') }}">
                        <a href="{!! url('dashboard/members/deleted') !!}">
                            <i class="fa fa-user-times"></i>
                            {{ trans('menus.admin.member.deleted') }}
                        </a>
                    </li>
                    @end
                </ul>
            </li>
            @end

            @permission('manage-alliances')
            <li class="{{ Active::pattern('dashboard/alliances/*') }} treeview">
                <a href="#">
                    <i class="fa phpdebugbar-fa-link"></i>
                    <span>{{ trans('menus.admin.alliances.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('dashboard/alliances*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('dashboard/alliances*', 'display: block;') }}">
                    @permission('create-alliances')
                    <li class="{{ Active::pattern('dashboard/alliances/create') }}">
                        <a href="{!! route('dashboard.alliances.create') !!}">
                            <i class="fa fa-plus-circle"></i>
                            {{ trans('menus.admin.alliances.add') }}
                        </a>
                    </li>
                    @end
                    <li class="{{ Active::pattern('dashboard/alliances') }}">
                        <a href="{!! route('dashboard.alliances.index') !!}">
                            <i class="fa fa-eye"></i>
                            {{ trans('menus.admin.alliances.view') }}
                        </a>
                    </li>
                    @permission('restore-alliances')
                    <li class="{{ Active::pattern('dashboard/alliances/deleted') }}">
                        <a href="{!! url('dashboard/alliances/deleted') !!}">
                            <i class="fa fa-minus-circle"></i>
                            {{ trans('menus.admin.alliances.deleted') }}
                        </a>
                    </li>
                    @end
                    @permission('create-alliances-group')
                    <li class="{{ Active::pattern('dashboard/alliances/group') }}">
                        <a href="{!! url('dashboard/alliances/group') !!}">
                            <i class="fa fa-group"></i>
                            {{ trans('menus.admin.alliances.group') }}
                        </a>
                    </li>
                    @end
                </ul>
            </li>
            @end

            @permission('manage-events')
            <li class="{{ Active::pattern('dashboard/events/*') }} treeview">
                <a href="#">
                    <i class="fa fa-calendar"></i>
                    <span>{{ trans('menus.admin.events.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('dashboard/events*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('dashboard/events*', 'display: block;') }}">
                    @permission('create-events')
                    <li class="{{ Active::pattern('dashboard/events/create') }}">
                        <a href="{!! route('dashboard.events.create') !!}">
                            <i class="fa fa-calendar-plus-o"></i>
                            {{ trans('menus.admin.events.add') }}
                        </a>
                    </li>
                    @end
                    <li class="{{ Active::pattern('dashboard/events/calendar') }}">
                        <a href="{!! route('dashboard.events.calendar') !!}">
                            <i class="fa fa-calendar"></i>
                            {{ trans('menus.admin.events.calendar') }}
                        </a>
                    </li>
                    <li class="{{ Active::pattern('dashboard/events') }}">
                        <a href="{!! route('dashboard.events.index') !!}">
                            <i class="fa fa-calendar"></i>
                            {{ trans('menus.admin.events.view') }}
                        </a>
                    </li>
                    @permission('restore-events')
                    <li class="{{ Active::pattern('dashboard/events/canceled') }}">
                        <a href="{!! url('dashboard/events/canceled') !!}">
                            <i class="fa fa-calendar-times-o"></i>
                            {{ trans('menus.admin.events.canceled') }}
                        </a>
                    </li>
                    @end
                    @permission('restore-events')
                    <li class="{{ Active::pattern('dashboard/events/deleted') }}">
                        <a href="{!! url('dashboard/events/deleted') !!}">
                            <i class="fa fa-calendar-times-o"></i>
                            {{ trans('menus.admin.events.deleted') }}
                        </a>
                    </li>
                    @end
                </ul>
            </li>
            @end
        </ul>
    </section>
</aside>
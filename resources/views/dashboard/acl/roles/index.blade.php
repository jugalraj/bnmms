@extends ('dashboard.layouts.master')

@section ('title', trans('labels.admin.acl.roles.header'))

@section('page-header')
    <h1>{{ trans('labels.admin.acl.roles.header') }}</h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.admin.acl.roles.header') }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Role</th>
                        <th>Permissions</th>
                        <th>Number of Users</th>
                        <th>Sort</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($roles as $role)
                            <tr>
                                <td>{!! $role->name !!}</td>
                                <td>
                                    @if ($role->all)
                                        <span class="label label-success">All</span>
                                    @else
                                        @if (count($role->permissions) > 0)
                                            <div style="font-size:.7em">
                                                @foreach ($role->permissions as $permission)
                                                    {!! $permission->name !!}<br/>
                                                @endforeach
                                            </div>
                                        @else
                                            <span class="label label-danger">None</span>
                                        @endif
                                    @endif
                                </td>
                                <td>{!! $role->users()->count() !!}</td>
                                <td>{!! $role->sort !!}</td>
                                <td>{!! $role->action_buttons !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="pull-left">
                {{ $roles->total() }} {{ trans_choice('labels.admin.acl.roles.table.total', $roles->total()) }}
            </div>

            <div class="pull-right">
                {{ $roles->render() }}
            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop

@extends ('dashboard.layouts.master')

@section ('title', 'Roles Management' . ' | ' . 'Create Roles')

@section('page-header')
    <h1>
        {{ trans('labels.admin.acl.roles.header') }}
        <small>{{ trans('labels.admin.acl.roles.create') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    {!! Html::style('assets\css\jstree\themes\default\style.min.css') !!}
@stop

@section('content')
    {!! Form::open(['route' => 'dashboard.roles.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-role']) !!}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.admin.acl.roles.create') }}</h3>

            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    {!! Form::label('slug', 'Slug', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'slug']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('name', 'Sort Order', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::input('number','sort', null, ['class' => 'form-control', 'placeholder' => 'Sort']) !!}
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-md-2 control-label">Select Permissions</label>
                    <div class="col-lg-10">
                        <div id="available-permissions">
                            <div class="row"><hr/>

                                <div class="col-lg-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle"></i>
                                            {!! trans('strings.dashboard.acl.permissions.associated-permissions-explanation') !!}
                                    </div><!--alert-->
                                </div><!--col-lg-12-->

                                <div class="col-lg-6">
                                    <p><strong>Grouped Permission</strong></p>

                                    @if ($groups->count())
                                        <div id="permission-tree">
                                            <ul>
                                                @foreach ($groups as $group)
                                                    <li>{!! $group->name !!}
                                                        @if ($group->permissions->count())
                                                            <ul>
                                                                @foreach ($group->permissions as $permission)
                                                                    <li id="{!! $permission->id !!}" data-dependencies="{!! json_encode($permission->dependencies->lists('dependency_id')->all()) !!}">

                                                                        @if ($permission->dependencies->count())
                                                                            <?php
                                                                            //Get the dependency list for the tooltip
                                                                            $dependency_list = [];
                                                                            foreach ($permission->dependencies as $dependency)
                                                                                array_push($dependency_list, $dependency->permission->name);
                                                                            $dependency_list = implode(", ", $dependency_list);
                                                                            ?>
                                                                            <a data-toggle="tooltip" data-html="true" title="<strong>{{ trans('labels.admin.acl.permissions.dependencies') }}:</strong> {!! $dependency_list !!}">{!! $permission->name !!} <small><strong>(D)</strong></small></a>
                                                                        @else
                                                                            {!! $permission->name !!}
                                                                        @endif

                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif

                                                        @if ($group->children->count())
                                                            <ul>
                                                                @foreach ($group->children as $child)
                                                                    <li>{!! $child->name !!}
                                                                        @if ($child->permissions->count())
                                                                            <ul> style="padding-left:40px;font-size:.8em">
                                                                                @foreach ($child->permissions as $permission)
                                                                                    <li id="{!! $permission->id !!}" data-dependencies="{!! json_encode($permission->dependencies->lists('dependency_id')->all()) !!}">
                                                                                        @if ($permission->dependencies->count())
                                                                                            <?php
                                                                                            //Get the dependency list for the tooltip
                                                                                            $dependency_list = [];
                                                                                            foreach ($permission->dependencies as $dependency)
                                                                                                array_push($dependency_list, $dependency->permission->name);
                                                                                            $dependency_list = implode(", ", $dependency_list);
                                                                                            ?>
                                                                                            <a data-toggle="tooltip" data-html="true" title="<strong>Dependencies:</strong> {!! $dependency_list !!}">{!! $permission->name !!} <small><strong>(#)</strong></small></a>
                                                                                        @else
                                                                                            {!! $permission->name !!}
                                                                                        @endif

                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        @endif
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @else
                                        <p>No Groups</p>
                                    @endif
                                </div><!--col-lg-6-->

                                <div class="col-lg-6">
                                    <p><strong>Ungrouped Permission</strong></p>

                                    @if ($permissions->count())
                                        @foreach ($permissions as $perm)
                                            <input type="checkbox" name="ungrouped[]" data-dependencies="{!! json_encode($perm->dependencies->lists('dependency_id')->all()) !!}" value="{!! $perm->id !!}" id="perm_{!! $perm->id !!}" /> <label for="perm_{!! $perm->id !!}">

                                                @if ($perm->dependencies->count())
                                                    <?php
                                                    //Get the dependency list for the tooltip
                                                    $dependency_list = [];
                                                    foreach ($perm->dependencies as $dependency)
                                                        array_push($dependency_list, $dependency->permission->name);
                                                    $dependency_list = implode(", ", $dependency_list);
                                                    ?>
                                                    <a style="color:black;text-decoration:none;" data-toggle="tooltip" data-html="true" title="<strong>Dependencies:</strong> {!! $dependency_list !!}">{!! $perm->name !!} <small><strong>(D)</strong></small></a>
                                                @else
                                                    {!! $perm->name !!}
                                                @endif

                                            </label><br/>
                                        @endforeach
                                    @else
                                        <p>There are no ungrouped permissions.</p>
                                    @endif
                                </div><!--col-lg-6-->
                            </div><!--row-->
                        </div><!--available permissions-->
                    </div><!--col-lg-3-->
                </div><!--form control-->

            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-right">
                    <input type="submit" class="btn btn-success" value="Create" />
                    <a href="{!! route('dashboard.roles.index') !!}" class="btn btn-danger">Cancel</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        {!! Form::hidden('permissions') !!}
    {!! Form::close() !!}
@stop

@section('after-scripts-end')
    {!! Html::script('assets/js/jstree.min.js') !!}
    {!! Html::script('assets/js/acl/role/script.js') !!}
@stop

@extends ('dashboard.layouts.master')

@section ('title', trans('labels.admin.acl.users.header'))

@section('page-header')
    <h1>
        {{ trans('labels.admin.acl.users.header') }}
        <small>{{ trans('labels.admin.acl.users.active') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.admin.acl.users.active') }}</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{{ trans('labels.admin.acl.users.table.id') }}</th>
                        <th>{{ trans('labels.admin.acl.users.table.name') }}</th>
                        <th>{{ trans('labels.admin.acl.users.table.email') }}</th>
                        <th>{{ trans('labels.admin.acl.users.table.confirmed') }}</th>
                        <th>{{ trans('labels.admin.acl.users.table.roles') }}</th>
                        <th class="visible-lg">{{ trans('labels.admin.acl.users.table.created') }}</th>
                        <th class="visible-lg">{{ trans('labels.admin.acl.users.table.last_updated') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{!! $user->id !!}</td>
                                <td>{!! $user->full_name !!}</td>
                                <td>{!! link_to("mailto:".$user->email, $user->email) !!}</td>
                                <td>{!! $user->confirmed_label !!}</td>
                                <td>
                                    @if ($user->roles()->count() > 0)
                                        @foreach ($user->roles as $role)
                                            {!! $role->name !!}<br/>
                                        @endforeach
                                    @else
                                        {{ trans('labels.general.none') }}
                                    @endif
                                </td>
                                <td class="visible-lg">{!! $user->created_at->diffForHumans() !!}</td>
                                <td class="visible-lg">{!! $user->updated_at->diffForHumans() !!}</td>
                                <td>{!! $user->action_buttons !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="pull-left">
                {!! $users->total() !!} {{ trans_choice('labels.admin.acl.users.table.total', $users->total()) }}
            </div>

            <div class="pull-right">
                {!! $users->render() !!}
            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop


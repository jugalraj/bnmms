@extends ('dashboard.layouts.master')

@section ('title', trans('labels.admin.acl.permissions.header') . ' | ' . trans('labels.admin.acl.permissions.create'))

@section('page-header')
    <h1>
        {{ trans('labels.admin.acl.permissions.header') }}
        <small>{{ trans('labels.admin.acl.permissions.create') }}</small>
    </h1>
@endsection

@section('content')
    {!! Form::open(['route' => 'dashboard.permissions.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.admin.acl.permissions.create') }}</h3>

            </div><!-- /.box-header -->

            <div class="box-body">
                <div>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel"">

                            <div class="form-group">
                                {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-10">
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                                </div>
                            </div><!--form control-->

                            <div class="form-group">
                                {!! Form::label('slug', 'Slug', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-10">
                                    {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'slug-for-name']) !!}
                                </div>
                            </div><!--form control-->

                            <div class="form-group">
                                {!! Form::label('group', 'Permission group', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-10">
                                    <select name="group" class="form-control">
                                        <option value="">None</option>

                                        @foreach ($groups as $group)
                                            <option value="{!! $group->id !!}">{!! $group->name !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><!--form control-->

                            <div class="form-group">
                                {!! Form::label('sort', 'Sort', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-10">
                                    {!! Form::text('sort', null, ['class' => 'form-control', 'placeholder' => 'Sort']) !!}
                                </div>
                            </div><!--form control-->

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Associated Roles</label>
                                <div class="col-lg-3">
                                    @if (count($roles) > 0)
                                        @foreach($roles as $role)
                                            <input type="checkbox" {{$role->id == 1 ? 'disabled checked' : ''}} value="{{$role->id}}" name="permission_roles[]" id="role-{{$role->id}}" /> <label for="role-{{$role->id}}">{!! $role->name !!}</label><br/>
                                        @endforeach
                                    @else
                                        No Roles to Set
                                    @endif
                                </div>
                            </div><!--form control-->

                        </div><!--general-->

                        <div style="padding-top:20px">

                            <div class="alert alert-info">
                                <i class="fa fa-info-circle"></i>
                                This section is where you specify that this permission depends on the user having one or more other permissions.<br/><br/>
                                For example: This permission may be <strong>create-user</strong>, but if the user doesn't also have <strong>view-backend</strong> and <strong>view-access-management</strong> permissions they will never be able to get to the <strong>Create User</strong> screen.
                            </div><!--alert-->

                            <div class="form-group">
                                <div class="col-lg-2">
                                    <h4>Dependencies</h4>
                                </div>
                                <br />
                                <div class="col-lg-11">
                                    @if (count($permissions))
                                        @foreach (array_chunk($permissions->toArray(), 10) as $perm)
                                            <div class="col-lg-3">
                                                <ul style="margin:0;padding:0;list-style:none;">
                                                    @foreach ($perm as $p)
                                                        <?php
                                                        //Since we are using array format to nicely display the permissions in rows
                                                        //we will just manually create an array of dependencies since we do not have
                                                        //access to the relationship to use the lists() function of eloquent
                                                        //but the relationships are eager loaded in array format now
                                                        $dependencies = [];
                                                        $dependency_list = [];
                                                        if (count($p['dependencies'])) {
                                                            foreach ($p['dependencies'] as $dependency) {
                                                                array_push($dependencies, $dependency['dependency_id']);
                                                                array_push($dependency_list, $dependency['permission']['name']);
                                                            }
                                                        }
                                                        $dependencies = json_encode($dependencies);
                                                        $dependency_list = implode(", ", $dependency_list);
                                                        ?>

                                                        <li><input type="checkbox" value="{{$p['id']}}" name="dependencies[]" data-dependencies="{!! $dependencies !!}" id="permission-{{$p['id']}}" /> <label for="permission-{{$p['id']}}" />

                                                            @if ($p['dependencies'])
                                                                <a style="color:black;text-decoration:none;" data-toggle="tooltip" data-html="true" title="<strong>Dependencies:</strong> {!! $dependency_list !!}">{!! $p['name'] !!} <small><strong>(D)</strong></small></a>
                                                            @else
                                                                {!! $p['name'] !!}
                                                                @endif

                                                                </label></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endforeach
                                    @else
                                        No permission to choose from.
                                    @endif
                                </div><!--col 3-->
                            </div><!--form control-->

                        </div><!--dependencies-->

                    </div><!--tab content-->

                </div><!--tabs-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-right">
                    <input type="submit" class="btn btn-success" value="Create" />
                    <a href="{!! route('dashboard.permissions.index') !!}" class="btn btn-danger">Cancel</a>
                </div>
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {!! Form::close() !!}
@stop

@section('after-scripts-end')
    {!! Html::script('assets/js/acl/dependency/script.js') !!}
@stop
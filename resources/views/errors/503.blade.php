 @extends('layouts.landingpage')

@section('content')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrappera">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 align="center">
            <a href="/"> {{ config('app.app_name') }} </a>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow"> 503</h2>
            <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
                <p>
                    Be right back.
                </p>
            </div>
        </div>
    </section>
</div>
@endsection
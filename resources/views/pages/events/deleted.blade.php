@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.events.header') }}
        <small>{{ trans('labels.admin.events.canceled') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
    <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="eventsTable" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>All Day Event</th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach($calendar_events as $calendar_event)
                                    <tr>
                                        <td>{{$calendar_event->id}}</td>
                                        <td>{{$calendar_event->title}}</td>
                                        <td>{{$calendar_event->start}}</td>
                                        <td>{{$calendar_event->end}}</td>
                                        <td>{{$calendar_event->is_all_day}}</td>
                                        <td class="text-right">
                                            @permission('undelete-events')
                                                <a href="{{route('dashboard.events.restore', $calendar_event->id)}}" class="btn btn-xs btn-success" name="restore_event"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Restore User"></i></a>
                                            @end

                                            @permission('permanently-delete-events')
                                                <a href="{{route('dashboard.events.delete-permanently', $calendar_event->id)}}" class="btn btn-xs btn-danger" name="delete_event_perm"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="Delete Permanently"></i></a>
                                            @end
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                            <a class="btn btn-success" href="{{ route('dashboard.events.create') }}">Create</a>
                        </div>
                    </div>
                    <div id="calendar"></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->
        </div>
    </div>
@endsection

@section('after-scripts-end')
    {!! Html::script('assets/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/js/dataTables.bootstrap.min.js') !!}
    <script>
        $(function () {
            $('#eventsTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": false,
                "autoWidth": false
            });
            $(function () {
                @permission('permanently-delete-events')
                    $("a[name='delete_event_perm']").click(function (e) {
                    e.preventDefault();

                    swal({
                        title: "Are you sure to delete this event permanently?",
                        tevents"Anywhere in the application that references this event\'s id will most likely error. Proceed atevents own risk. This can not be un-done.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Continue",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: false
                    }, function (isConfirmed) {
                        if (isConfirmed) {
                            window.location = $("a[name='delete_event_perm']").attr('href');
                        }
                    });
                });
                @end
    
                @permission('reactivate-events')
                    $("a[name='restore_event']").click(function (e) {
                    e.preventDefault();

                    swal({
                        title: "Are you Sure to restore this event?",
                        text: "Restoreventss event to its original state?",
               events    type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Continue",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: false
                    }, function (isConfirmed) {
                        if (isConfirmed) {
                            window.location = $("a[name='restore_event']").attr('href');
                        }
                    });
                });
                @end
            });
        });
    </script>
@endsection
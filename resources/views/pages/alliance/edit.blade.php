@extends('dashboard.layouts.master')

@section ('title', trans('labels.dashboard.alliances.header') . ' | ' . trans('labels.dashboard.alliances.create'))

@section('page-header')
    <h1>
        {{ trans('labels.dashboard.alliances.header') }}
        <small>{{ trans('labels.dashboard.alliances.edit') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            {!! Form::model($alliance, ['method' => 'PUT','route' => ['dashboard.alliances.update',$alliance->id], 'class' => 'form-horizontal']) !!}
                @include('pages.alliance.partials._alliances_EditForm')
            {!! Form::close() !!}
        </div>
        <div class="box-footer"></div>
    </div>
@endsection

@section('after-scripts-end')

@endsection
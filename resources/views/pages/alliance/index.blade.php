@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.dashboard.alliances.header') }}
        <small>{{ trans('labels.dashboard.alliances.active') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title">

            </div>
            @permission('create-alliances')
            <div class="box-tools pull-right">
                <a class="btn btn-xs btn-primary" href="{{ route('dashboard.alliances.create') }}">
                    <i class="fa fa-user-plus"></i>
                </a>
            </div>
            @end
        </div>
        <div class="box-body">
            <table id="allianceTable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Name of Person</th>
                    <th>Organization</th>
                    <th>Phone No.</th>
                    <th>Email</th>
                    <th>Website</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($alliances as $alliance)
                    <tr>
                        <td><a href="{{ route('dashboard.alliances.show', $alliance->id) }}">{{ $alliance->name }}</a>
                        </td>

                        <td>{{ $alliance->organization }}</td>
                        <td>{{ $alliance->mobile }}</td>
                        <td>{{ $alliance->email }}</td>
                        <td>{{ Html::link('http://'.$alliance->website, $alliance->website, array('target' => '_blank')) }}</td>
                        <td>
                            <div class="action-buttons pull-right">{!! $alliance->action_buttons !!}</div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('after-scripts-end')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#allianceTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": false,
                "autoWidth": false
            });
        });
    </script>
@endsection
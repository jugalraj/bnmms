@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.dashboard.alliances.header') }}
        <small>{{ trans('labels.dashboard.alliances.deleted') }}</small>
    </h1>
@endsection

@section('content')
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title">
                {!! Form::open(['method' => 'GET', 'action' => ['AlliancesController@index'], 'class'=> "navbar-form navbar-left"]) !!}
                <div class="form-group">
                    <span class="icon"><i class="fa fa-search"></i></span>
                    {!! Form::input('search','search',null,['id'=> 'searchBox', 'class'=>'form-control', 'placeholder'=>'Search alliances...']) !!}
                </div>
                <input type="submit" hidden/>
                {!! Form::close() !!}
            </div>
            @permission('create-alliances')
            <div class="box-tools pull-right">
                <a class="btn btn-xs btn-primary" href="{{ route('dashboard.alliances.create') }}">
                    <i class="fa fa-user-plus"></i>
                </a>
            </div>
            @end
        </div>
        <div class="box-body">
            <table id="allianceTable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Name of Person</th>
                    <th>Organization</th>
                    <th>Phone No.</th>
                    <th>Email</th>
                    <th>Website</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($alliances as $alliance)
                    <tr>
                        <td><a href="{{ route('dashboard.alliances.show', $alliance->id) }}">{{ $alliance->name }}</a>
                        </td>

                        <td>{{ $alliance->organization }}</td>
                        <td>{{ $alliance->mobile }}</td>
                        <td>{{ $alliance->email }}</td>
                        <td>{{ Html::link('http://'.$alliance->website, $alliance->website, array('target' => '_blank')) }}</td>
                        <td>
                            @permission('undelete-alliances')
                                <a href="{{route('dashboard.alliances.restore', $alliance->id)}}" class="btn btn-xs btn-success" name="restore_alliance"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Restore User"></i></a>
                            @end

                            @permission('permanently-delete-alliances')
                                <a href="{{route('dashboard.alliances.delete-permanently', $alliance->id)}}" class="btn btn-xs btn-danger" name="delete_alliance_perm"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="Delete Permanently"></i></a>
                            @end
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer ">
            <div class="pull-left">
                {{--{!! $alliances->total() !!} {{ trans_choice('labels.dashboard.alliances.total', $alliances->total()) }}--}}
            </div>
        </div>

        <div class="pull-right">
            {!! $alliances->render() !!}
        </div>
    </div>
@endsection

@section('after-scripts-end')
    <script>
        $(function () {
            @permission('permanently-delete-alliances')
                $("a[name='delete_alliance_perm']").click(function (e) {
                e.preventDefault();

                swal({
                    title: "Are you sure to delete this alliance permanently?",
                    text: "Anywhere in the application that references this alliance\'s id will most likely error. Proceed at your own risk. This can not be un-done.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Continue",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false
                }, function (isConfirmed) {
                    if (isConfirmed) {
                        window.location = $("a[name='delete_alliance_perm']").attr('href');
                    }
                });
            });
            @end

            @permission('reactivate-alliances')
                $("a[name='restore_alliance']").click(function (e) {
                e.preventDefault();

                swal({
                    title: "Are you Sure to restore this alliance?",
                    text: "Restore this alliance to its original state?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Continue",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false
                }, function (isConfirmed) {
                    if (isConfirmed) {
                        window.location = $("a[name='restore_alliance']").attr('href');
                    }
                });
            });
            @end
        });
    </script>
@stop

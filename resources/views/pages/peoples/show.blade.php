@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.peoples.header') }}
        <small>{{ trans('labels.admin.peoples.show') }}</small>
    </h1>
@endsection

@section('content')

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $people->getFullName() }}</h3>
                <div class="pull-right">
                    {!!  $people->action_buttons !!}
                </div>
            </div>
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Personal Detail</a></li>
                        <li class="pull-right"><a href="#" class="text-muted"></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            @include('pages.peoples.partials._people_personal_details')
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
            </div>
        </div>
@endsection

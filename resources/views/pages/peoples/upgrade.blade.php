@extends('dashboard.layouts.master')
@section ('title', trans('labels.admin.peoples.header') . ' | ' . trans('labels.admin.peoples.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.admin.peoples.header') }}
        <small>{{ trans('labels.admin.peoples.edit') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/nepali.datepicker.v2.1.min.css') }}">
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            {!! Form::model($people, ['method' => 'PUT', 'route' => ['dashboard.peoples.upgrade',$people->id], 'class' => 'form-horizontal', 'id' => 'peoplesForm' , 'files'=>'true']) !!}
            @include('pages.peoples.partials.makeMember_persnoalInfo_CreateForm')
            <hr/>
            @include('pages.peoples.partials._addressInfo_CreateForm')
            <hr/>
            @include('pages.peoples.partials._membershipInfo_CreateForm')
            <hr/>
            @include('pages.peoples.partials._paymentInfo_CreateForm')

            <div class="form-group">
                <div class="pull-right col-xs-10 ">
                    <button type="reset" class="btn btn-primary">
                        <i class="fa fa-btn fa-sign-in"></i> Reset
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-sign-in"></i> Update
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="box-footer"></div>
    </div>
@endsection

@section('after-scripts-end')
    {!! Html::script('assets/js/member.js') !!}
    {!! Html::script('assets/js/jquery.validate.js') !!}
    {!! Html::script('assets/js/createMemberForm.js') !!}
    {!! Html::script('assets/js/datepicker.js') !!}
    {!! Html::script('assets/js/nepali.datepicker.v2.1.min.js') !!}
    <script>
        var APP_URL = {!! json_encode(url('/')) !!};

        function getPermVDC(val, memVdc) {
            $.ajax({
                type: "GET",
                url: APP_URL + "/dashboard/district/" + val + "/vdcs.json",
                data: val,
                success: function (data) {
                    $("#perm_vdc").html(data);
                    console.log(response);
                }
            });
        }

        function getCurrVDC(val, memVdc) {
            $.ajax({
                type: "GET",
                url: APP_URL + "/dashboard/district/" + val + "/vdcs.json",
                data: val,
                success: function (data) {
                    $("#curr_vdc").html(data);
                    console.log(response);
                }
            });
        }
        $(document).ready(function () {
            $('#np_date_of_birth').nepaliDatePicker({
                npdMonth: true,
                npdYear: true,
                ndpEnglishInput: 'date_of_birth'
            });

            $('#date_of_birth').change(function () {
                $('#np_date_of_birth').val(AD2BS($('#date_of_birth').val()));
            });

        });
        $('#date_of_birth').datepicker({
            autoClose: true,
        });
        $('#registration_date').datepicker({
            autoClose: true,
        });
        $('#renewal_date').datepicker({
            autoClose: true,
        });
        $('#payment_date').datepicker({
            autoClose: true
        });
    </script>
@endsection
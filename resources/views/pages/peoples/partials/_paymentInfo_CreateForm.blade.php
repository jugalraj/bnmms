<div class="row">
    <div class="col-md-6">
        <!-- Payment Date Form Text Input -->
        <div class="form-group{{ $errors->has('payment_date') ? ' has-error' : '' }}">
            {!! Form::label('payment_date','Payment Date ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">
                {!! Form::text('payment_date','',['class'=>'form-control', 'required']) !!}

                @if ($errors->has('payment_date'))
                    <span class="help-block">
                    <strong>{{ $errors->first('payment_date') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <!-- Receipt Id Form Text Input -->
        <div class="form-group{{ $errors->has('receipt_id') ? ' has-error' : '' }}">
            {!! Form::label('receipt_id','Receipt Id ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">
                {!! Form::input('number','receipt_id',null,['class'=>'form-control',  'required']) !!}

                @if ($errors->has('receipt_id'))
                    <span class="help-block">
                    <strong>{{ $errors->first('receipt_id') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <!-- Payment Modes Form Text Input -->
        <div class="form-group{{ $errors->has('payment_modes') ? ' has-error' : '' }}">
            {!! Form::label('payment_modes','Payment Mode ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">
                <select name="payment_modes" id="payment_modes" class="form-control" required>
                    <option value="Cash">Cash</option>
                    <option value="Bank Transfer">Bank Transfer/Deposit</option>
                    <option value="Remittance">Remittance</option>
                    <option value="E-sewa">E-sewa</option>
                    <option value="nPAY">nPAY</option>
                </select>

                @if ($errors->has('payment_modes'))
                    <span class="help-block">
                    <strong>{{ $errors->first('payment_modes') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <!-- Payment Amount Form Text Input -->
        <div class="form-group{{ $errors->has('payment_amount') ? ' has-error' : '' }}">
            {!! Form::label('payment_amount','Payment Amount ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">
                {!! Form::input('number','payment_amount',null,['class'=>'form-control', 'step'=>"0.01",  'required']) !!}

                @if ($errors->has('payment_amount'))
                    <span class="help-block">
                    <strong>{{ $errors->first('payment_amount') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('membership_year') ? ' has-error' : '' }}" id="membership_year">
            {!! Form::label('membership_year','Membership For Year ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">
                <select name="membership_year" id="membership_year" class="form-control">
                    <option>{{ $nepDate->get_nep_date('year') - 1 }}</option>
                    <option selected="selected">{{ $nepDate->get_nep_date('year') }}</option>
                    <option>{{ $nepDate->get_nep_date('year') + 1 }}</option>
                </select>
                <span>(In Nepali Date)</span>

                @if ($errors->has('membership_year'))
                    <span class="help-block">
                    <strong>{{ $errors->first('membership_year') }}</strong>
                </span>
                @endif
            </div>
        </div>

    </div>
    <div class="col-md-6">

        <!-- Received By Form Text Input -->
        <div class="form-group{{ $errors->has('received_by_user') ? ' has-error' : '' }}">
            {!! Form::label('received_by_user','Received By ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">
                {!! Form::text('received_by_user', Auth::user()->full_name,['class'=>'form-control', 'disabled']) !!}

                @if ($errors->has('received_by'))
                    <span class="help-block">
                    <strong>{{ $errors->first('received_by_user') }}</strong>
                </span>
                @endif
            </div>
        </div>


        <!-- Hidden Value for received by Form Text Input -->
    {!! Form::input('hidden','received_by',Auth::user()->id,['class'=>'form-control']) !!}

    <!-- Comments Form Text Input -->
        <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }}">
            {!! Form::label('comments','Comments ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::textarea('comments',null,['class'=>'form-control', 'rows' => '5']) !!}

                @if ($errors->has('comments'))
                    <span class="help-block">
                    <strong>{{ $errors->first('comments') }}</strong>
                </span>
                @endif
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {{-- Permanent Address Section --}}
        <div class="form-group">
            {!! Form::label('permanent_address','Permanent Address ',['class' =>'col-md-2 control-label']) !!}
        </div>

        <!-- Permanent District Form Text Input -->
        <div class="form-group">
            <tr>
                <td>
                    <!-- District Form Text Input -->
                    <div class="{{ $errors->has('perm_address') ? ' has-error' : '' }}">
                        {!! Form::label('perm_district','District ',['class' =>'col-md-2 control-label']) !!}

                        <div class="col-md-3">
                            {!! Form::select('perm_district' ,App\Models\Dashboard\Abode::distinct()->lists('district','district')->toArray(), null, ['class'=>'form-control', 'onChange'=> 'getPermVDC(this.value)' , 'onfocus' => 'getPermVDC(this.value)']) !!}
                            @if ($errors->has('perm_district'))
                                <span class="help-block">
                                                <strong>{{ $errors->first('perm_district') }}</strong>
                                            </span>
                            @endif
                        </div>
                    </div>
                </td>
                <td>
                    <!-- VDC Form Text Input -->
                    <div class="{{ $errors->has('perm_vdc') ? ' has-error' : '' }}">
                        {!! Form::label('perm_vdc','VDC ',['class' =>'col-md-1 control-label']) !!}

                        <div class="col-md-3">
                            {!! Form::select('perm_vdc' ,array($people->perm_vdc => $people->perm_vdc),null, ['class'=>'form-control']) !!}

                            @if ($errors->has('perm_vdc'))
                                <span class="help-block">
                                                <strong>{{ $errors->first('perm_vdc') }}</strong>
                                            </span>
                            @endif
                        </div>
                    </div>
                </td>
                <td>
                    <!-- Ward Form Text Input -->
                    <div class="{{ $errors->has('perm_address') ? ' has-error' : '' }}">
                        {!! Form::label('perm_ward','Ward ',['class' =>'col-md-1 control-label']) !!}

                        <div class="col-md-1">
                            {!! Form::selectRange('perm_ward', 1, 35, $people->perm_ward, ['class'=>'form-control']) !!}

                            @if ($errors->has('perm_ward'))
                                <span class="help-block">
                                                <strong>{{ $errors->first('perm_ward') }}</strong>
                                            </span>
                            @endif
                        </div>
                    </div>
                </td>
            </tr>
        </div>

        {{-- Current Address Section --}}
        <div class="form-group">
            {!! Form::label('current_address','Current Address ',['class' =>'col-md-2 control-label']) !!}
        </div>

        <!-- Country Form Text Input -->
        <div class="form-group{{ $errors->has('curr_country') ? ' has-error' : '' }}">
            {!! Form::label('curr_country','Country ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-3">
                {{ Form::select('curr_country', array_keys(\App\Http\Utilities\Country::getAllCountries()), $people->curr_country, ['class' => 'form-control']) }}

                @if ($errors->has('curr_country'))
                    <span class="help-block">
                                    <strong>{{ $errors->first('curr_country') }}</strong>
                                </span>
                @endif
            </div>
        </div>


        <div class="nepal_address">
            <tr>

                <div class="form-group">
                    <td>
                        <!-- Current District Form Text Input -->
                        <div id="curr_district" class="{{ $errors->has('curr_district') ? ' has-error' : '' }}">
                            {!! Form::label('curr_district','District ',['class' =>'col-md-2 control-label']) !!}

                            <div class="col-md-3">
                                {!! Form::select('curr_district' ,App\Models\Dashboard\Abode::distinct()->lists('district','district')->toArray(), null, ['class'=>'form-control', 'onChange'=> 'getCurrVDC(this.value)']) !!}

                                @if ($errors->has('curr_district'))
                                    <span class="help-block">
                                                    <strong>{{ $errors->first('curr_district') }}</strong>
                                                </span>
                                @endif
                            </div>
                        </div>
                    </td>
                    <td>
                        <!-- VDC Form Text Input -->
                        <div id="currVDC" class="{{ $errors->has('curr_vdc') ? ' has-error' : '' }}">
                            {!! Form::label('curr_vdc','VDC ',['class' =>'col-md-1 control-label']) !!}

                            <div class="col-md-3">
                                {!! Form::select('curr_vdc' ,array($people->curr_vdc => $people->curr_vdc ),null, ['class'=>'form-control']) !!}

                                @if ($errors->has('curr_vdc'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('curr_vdc') }}</strong>
                                            </span>
                                @endif
                            </div>
                        </div>
                    </td>
                    <td>
                        <!-- Ward Form Text Input -->
                        <div id="curr_ward" class="{{ $errors->has('curr_ward') ? ' has-error' : '' }}">
                            {!! Form::label('curr_ward','Ward ',['class' =>'col-md-1 control-label']) !!}

                            <div class="col-md-1">
                                {!! Form::selectRange('curr_ward', 1, 35, null, ['class'=>'form-control']) !!}

                                @if ($errors->has('curr_ward'))
                                    <span class="help-block">
                                                        <strong>{{ $errors->first('curr_ward') }}</strong>
                                                    </span>
                                @endif
                            </div>
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div id="tole" class="form-group{{ $errors->has('tole') ? ' has-error' : '' }}">
                    {!! Form::label('tole','Tole ',['class' =>'col-md-2 control-label']) !!}

                    <div class="col-md-2">
                        {!! Form::text('tole',null,['class'=>'form-control', 'required']) !!}

                        @if ($errors->has('tole'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tole') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </tr>
        </div>


        <div class="foreign_address">
            <div class="row">
                <div class="col-md-12">
                    <tr>
                        <div class="form-group">
                            <td>

                                <!-- State Form Text Input -->
                                <div id="state" class="{{ $errors->has('state') ? ' has-error' : '' }}">
                                    {!! Form::label('state','State ',['class' =>'col-md-2 control-label']) !!}

                                    <div class="col-md-2">
                                        {!! Form::text('state',null,['class'=>'form-control']) !!}

                                        @if ($errors->has('state'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('state') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </td>
                            <td>
                                <!-- Zip/Postal Code Form Text Input -->
                                <div id="zip_or_postal" class="{{ $errors->has('zip_or_postal') ? ' has-error' : '' }}">
                                    {!! Form::label('zip_or_postal','Zip/Postal Code ',['class' =>'col-md-2 control-label']) !!}

                                    <div class="col-md-2">
                                        {!! Form::input('number','zip_or_postal',null,['class'=>'form-control']) !!}

                                        @if ($errors->has('zip_or_postal'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('zip_or_postal') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </td>
                            <td>
                                <!-- City Form Text Input -->
                                <div id="city" class="{{ $errors->has('city') ? ' has-error' : '' }}">
                                    {!! Form::label('city','City ',['class' =>'col-md-2 control-label']) !!}

                                    <div class="col-md-2">
                                        {!! Form::text('city',null,['class'=>'form-control']) !!}

                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('city') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </div>
                    </tr>
                    <tr>
                        <div class="form-group">
                            <td>
                                <!-- Street Form Text Input -->
                                <div id="street" class="{{ $errors->has('street') ? ' has-error' : '' }}">
                                    {!! Form::label('street','Street ',['class' =>'col-md-2 control-label']) !!}

                                    <div class="col-md-2">
                                        {!! Form::text('street',null,['class'=>'form-control']) !!}

                                        @if ($errors->has('street'))
                                            <span class="help-block">
                                                            <strong>{{ $errors->first('street') }}</strong>
                                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </td>
                            <td>
                                <!-- House Number Form Text Input -->
                                <div id="house_number"
                                     class="form-group{{ $errors->has('house_number') ? ' has-error' : '' }}">
                                    {!! Form::label('house_number','House Number ',['class' =>'col-md-2 control-label']) !!}

                                    <div class="col-md-2">
                                        {!! Form::input('number','house_number',null,['class'=>'form-control']) !!}

                                        @if ($errors->has('house_number'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('house_number') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </div>
                    </tr>
                </div>
            </div>
        </div>
    </div>
</div>
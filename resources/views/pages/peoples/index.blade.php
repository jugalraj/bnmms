@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.peoples.header') }}
        <small>{{ trans('labels.admin.peoples.active') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title">

            </div>
            @permission('create-peoples')
            <div class="box-tools pull-right">
                <a class="btn btn-xs btn-primary" href="{{ route('dashboard.peoples.create') }}">
                    <i class="fa fa-user-plus"></i> Add People
                </a>
            </div>
            @end
        </div>
        <div class="box-body">
            <table id="peoplesTable" class="table table-striped">
                <thead>
                <tr>
                    <th>Full Name</th>
                    <th>Association Type</th>
                    <th>Current Address</th>
                    <th>Email</th>
                    <th>Contact No</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($peoples as $people)
                    <tr>
                        <td>
                            <a href="{{ route('dashboard.peoples.show', $people->id) }}"> {{ $people->getFullName() }}</a>
                        </td>
                        <td>{{ $people->getAssociation() }}</td>
                        @if($people->curr_country == '0')
                            <td>{{ $people->curr_vdc.' '.$people->curr_ward.', '.$people->curr_district }}</td>
                        @else
                            <td>{{ $people->city.', '.\App\Http\Utilities\Country::getCountry($people->curr_country) }}</td>
                        @endif
                        <td>{{ $people->email }}</td>
                        <td>{{ $people->mobile_no }}</td>
                        <td>
                            <div class="action-buttons pull-right">{!! $people->action_buttons !!}</div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('after-scripts-end')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#peoplesTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endsection
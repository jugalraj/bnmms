<div class="row">
    <div class="col-md-12">
        <!-- Registration Date Form Text Input -->
        <div class="form-group{{ $errors->has('registration_date') ? ' has-error' : '' }}">
            {!! Form::label('registration_date','Registration Date ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-3">
                {!! Form::input('date','registration_date', $member->membership->registration_date,['class'=>'form-control', 'disabled' =>'disabled'], 'required') !!}

                @if ($errors->has('registration_date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('registration_date') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        {{--Membership Type Form Radio Input --}}
        <div class="form-group{{ $errors->has('membership_type') ? ' has-error' : '' }}">
            {!! Form::label('membership_type','Membership Type ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-4">
                <div class="radio">
                    <label>
                        <input name="membership_type" value="A" id="membership_type"
                               type="radio" disabled {{ $member->membership_type == 'A' ? 'checked' : '' }} /> Active
                    </label>
                    <label>
                        <input name="membership_type" value="G" id="membership_type"
                               type="radio" disabled {{ $member->membership_type == 'G' ? 'checked' : '' }} /> General
                    </label>
                </div>

                @if ($errors->has('membership_type'))
                    <span class="help-block">
                                    <strong>{{ $errors->first('membership_type') }}</strong>
                                </span>
                @endif
            </div>
        </div>

        <!-- Verified Document Form Text Input -->
        <div class="form-group{{ $errors->has('verified_document') ? ' has-error' : '' }}">
            {!! Form::label('verified_document','Verified Document ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-3">

                {{ Form::select('verified_document', ['Citizenship' => 'Citizenship', 'Passport' => 'Passport','Driving Liscense' =>'Driving Liscense'], $member->membership->verified_document, ['class' => 'form-control']) }}

                @if ($errors->has('verified_document'))
                    <span class="help-block">
                        <strong>{{ $errors->first('verified_document') }}</strong>
                                </span>
                @endif
            </div>
        </div>

        <!-- Document Number Form Text Input -->
        <div class="form-group{{ $errors->has('document_number') ? ' has-error' : '' }}">
            {!! Form::label('document_number','Document Number ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-3">
                {!! Form::input('number', 'document_number', $member->membership->document_number,['class'=>'form-control'], 'required') !!}

                @if ($errors->has('document_number'))
                    <span class="help-block">
                                    <strong>{{ $errors->first('document_number') }}</strong>
                                </span>
                @endif
            </div>
        </div>
    </div>
</div>


<table class="table">
    <tbody>
        <tr>
            <td><label>Registration ID </label></td>
            <td>{{ $member->membership_type.$member->registration_id }}</td>
            <td></td>
        </tr>
        <tr>
            <td><label>Registered on </label></td>
            <td>{{ toNepaliDate($member->membership->registration_date) }}</td>
            <td></td>
        </tr>
        <tr>
            <td><label for="">Membership Year</label></td>
            <td>{{ $member->membership->membership_year }}</td>
        </tr>
        <tr>
            <td><label>Expiry Date </label></td>
            <td>{{ $member->membership->renewal_date }}</td>
        </tr>
        <tr>
            <td><label>Verified Document </label></td>
            <td>{{ ucfirst($member->membership->verified_document) }}</td>
        </tr>
        <tr>
            <td><label>Document No.</label></td>
            <td>{{ $member->membership->document_number }}</td>
        </tr>
    </tbody>
</table>

<table class="table">
    <tbody>
        <tr>
            <td><label>Date of Birth: </label></td>
            <td>{{ $member->date_of_birth }}</td>
            <td><label>Age: </label>{{ ' '.$member->getAge() }}</td>
            @if($member->photo_path)
                <td rowspan="4"><img src="{{ $member->photo_path }}" height="150" width="200" /> </td>
            @else
                <td></td>
            @endif

        </tr>
        <tr>
            <td><label>Gender: </label></td>
            <td>{{ $member->gender }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Permanent Address: </label></td>
            <td>{{ $member->perm_vdc.' '.$member->perm_ward.', '.$member->perm_district }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Current Address: </label></td>
            @if($member->curr_country == '0')
            <td>{{ $member->curr_vdc.' '.$member->curr_ward.', '.$member->curr_district }}</td>
            @else
            <td>{{ $member->city.', '.$member->curr_country }}</td>
            @endif
            <td></td>
        </tr>
        <tr>
            <td><label>Email ID: </label></td>
            <td>{{ $member->email }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Phone no: </label></td>
            <td>{{ $member->contact_no }}</td>
            <td><label>Mobile no: </label></td>
            <td>{{ $member->mobile_no }}</td>
        </tr>
        <tr>
            <td><label>Qualification: </label></td>
            <td>{{ $member->qualification}}</td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>

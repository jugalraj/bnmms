<div class="row">
    <div class="col-md-12">
        <!-- First Name Form Text Input -->
        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            {!! Form::label('','First Name ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-2">
                {!! Form::text('first_name',null,['class'=>'form-control', 'required']) !!}

                @if ($errors->has('first_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                @endif
            </div>

            <!-- Middle Name Form Text Input -->
            {!! Form::label('middle_name','Middle Name ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-2">
                {!! Form::text('middle_name',null,['class'=>'form-control']) !!}

                @if ($errors->has('middle_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('middle_name') }}</strong>
                    </span>
                @endif
            </div>

            <!-- Last Name Form Text Input -->
            {!! Form::label('last_name','Last Name ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-2">
                {!! Form::text('last_name',null,['class'=>'form-control', 'required']) !!}

                @if ($errors->has('last_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <!-- Date of Birth Form Text Input -->
        <div class="form-group{{ $errors->has('np_date_of_birth') ? ' has-error' : '' }}">
            {!! Form::label('np_date_of_birth','Date of Birth ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-2">
                {!! Form::text('np_date_of_birth',null,['class'=>'form-control nepali-calendar','id' => 'np_date_of_birth']) !!}
                <span>(In Nepali Date)</span>

                @if ($errors->has('np_date_of_birth'))
                    <span class="help-block"><strong>{{ $errors->first('np_date_of_birth') }}</strong></span>
                @endif
            </div>
            <div class="col-md-2">
                {!! Form::text('date_of_birth',null,['class'=>'form-control','id' => 'date_of_birth']) !!}
                <span>(In English Date)</span>
            </div>
        </div>

        {{--Gender Form Radio Input --}}
        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
            {!! Form::label('gender','Gender ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-4">
                <div class="radio">
                    <label>
                        <input name="gender" value="Male" id="gender"
                               type="radio" {{ $member->gender == 'Male' ? 'checked' : '' }} /> Male
                    </label>
                    <label>
                        <input name="gender" value="Female" id="gender"
                               type="radio" {{ $member->gender == 'Female' ? 'checked' : '' }} /> Female
                    </label>
                    <label>
                        <input name="gender" value="3rd Gender" id="gender"
                               type="radio" {{ $member->gender == '3rd Gender' ? 'checked' : '' }}> 3rd
                        Gender
                    </label>
                </div>

                @if ($errors->has('gender'))
                    <span class="help-block">
                        <strong>{{ $errors->first('gender') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <!-- Email Form Text Input -->
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            {!! Form::label('email','Email ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-3">
                {!! Form::input('email','email',null,['class'=>'form-control']) !!}

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <!-- Phone No Form Text Input -->
        <div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
            {!! Form::label('contact_no','Phone No ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-3">
                {!! Form::input('phone','contact_no',null,['class'=>'form-control']) !!}

                @if ($errors->has('contact_no'))
                    <span class="help-block">
                        <strong>{{ $errors->first('contact_no') }}</strong>
                    </span>
                @endif
            </div>

            {!! Form::label('mobile_no','Mobile No ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-3">
                {!! Form::input('phone','mobile_no',null,['class'=>'form-control']) !!}

                @if ($errors->has('mobile_no'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mobile_no') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('profession') ? ' has-error' : '' }}">
            {!! Form::label('profession','Profession ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-3">
                {!! Form::text('profession',null,['class'=>'form-control', 'required']) !!}

                @if ($errors->has('profession'))
                    <span class="help-block">
                        <strong>{{ $errors->first('profession') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        {{--<div class="form-group{{ $errors->has('members_photo') ? ' has-error' : '' }}">--}}
        {{--{!! Form::label('members_photo','Photo ',['class' =>'col-md-2 control-label']) !!}--}}

        {{--<div class="col-md-4">--}}
        {{--{!! Form::input('file','members_photo',null,['class'=>'form-control'], 'required') !!}--}}

        {{--@if ($errors->has('members_photo'))--}}
        {{--<span class="help-block">--}}
        {{--<strong>{{ $errors->first('members_photo') }}</strong>--}}
        {{--</span>--}}
        {{--@endif--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>
</div>

@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.members.header') }}
        <small>{{ trans('labels.admin.members.active') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection


@section('content')
    <!-- Default box -->

    <div class="box">
        <div class="box-header with-border">
            <div class="box-title">

            </div>
            @permission('create-members')
            <div class="box-tools pull-right">
                <a class="btn btn-xs btn-primary" href="{{ route('dashboard.members.create') }}">
                    <i class="fa fa-user-plus"></i> Add Member
                </a>
            </div>
            @end
        </div>
        <div class="box-body">
            <table id="membersTable" class="table table-striped">
                <thead>
                <tr>
                    <th>Full Name</th>
                    <th>Country</th>
                    <th>Current Address</th>
                    <th>Contact No</th>
                    <th>Email</th>
                    <th>Profession</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($members as $member)
                    <tr>
                        <td>
                            <a href="{{ route('dashboard.members.show', $member->id) }}"> {{ $member->getFullName() }}</a>
                        </td>
                        <td>
                            @if($member->curr_country == '0')
                                Nepal
                            @else
                                {{ \App\Http\Utilities\Country::getCountry($member->curr_country) }}
                            @endif
                        </td>
                        <td>
                            @if($member->curr_country == '0')
                                {{ $member->curr_vdc.' '.$member->curr_ward.', '.$member->curr_district }}
                            @else
                                {{ $member->city.', '.\App\Http\Utilities\Country::getCountry($member->curr_country) }}
                            @endif
                        </td>
                        <td>{{ $member->mobile_no }}</td>

                        <td>{{ $member->email }}</td>
                        <td>{{ $member->profession }}</td>
                        <td>
                            <div class="action-buttons pull-right">{!! $member->action_buttons !!}</div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('after-scripts-end')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#membersTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endsection
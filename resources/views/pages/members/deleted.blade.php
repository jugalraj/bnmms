@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.members.header') }}
        <small>{{ trans('labels.admin.members.deleted') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title">

            </div>
            @permission('create-members')
            <div class="box-tools pull-right">
                <a class="btn btn-xs btn-primary" href="{{ route('dashboard.members.create') }}">
                    <i class="fa fa-user-plus"></i>
                </a>
            </div>
            @end
        </div>
        <div class="box-body">
            <table id="membersTable" class="table table-striped">
                <thead>
                <tr>
                    <th>Full Name</th>
                    <th>Current Address</th>
                    <th>Contact No</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($members as $member)
                    <tr>
                        <td>
                            <a href="{{ route('dashboard.members.show', $member->id) }}"> {{ $member->getFullName() }}</a>
                        </td>
                        @if($member->curr_country == '0')
                            <td>{{ $member->curr_vdc.' '.$member->curr_ward.', '.$member->curr_district }}</td>
                        @else
                            <td>{{ $member->city.', '.\App\Http\Utilities\Country::getCountry($member->curr_country) }}</td>
                        @endif
                        <td>{{ $member->contact_no }}</td>
                        <td>{{ $member->membership->showMembershipStatus() }}</td>
                        <td>
                            <div class="pull-right">
                                @permission('restore-members')
                                <a href="{{ route('dashboard.members.restore', $member->id) }}"
                                   class="btn btn-xs btn-success" name="restore_user"><i class="fa fa-refresh"
                                                                                         data-toggle="tooltip"
                                                                                         data-placement="top"
                                                                                         title="Restore User"></i></a>
                                @end

                                @permission('permanently-delete-members')
                                <a href="{{ route('dashboard.members.delete-permanently', $member->id) }}"
                                   class="btn btn-xs btn-danger" name="delete_member_perm"><i class="fa fa-times"
                                                                                              data-toggle="tooltip"
                                                                                              data-placement="top"
                                                                                              title="Delete Permanently"></i></a>
                                @end
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer ">
            <div class="pull-left">
                {{--{!! $members->total() !!} {{ trans_choice('labels.admin.members.total', $members->total()) }}--}}
            </div>
        </div>

        <div class="pull-right">
            {!! $members->render() !!}
        </div>
    </div>
@endsection

@section('after-scripts-end')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#membersTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
    <script>
        $(function () {
            @permission('permanently-delete-members')
                $("a[name='delete_member_perm']").click(function (e) {
                e.preventDefault();

                swal({
                    title: "Are you sure to delete this member permanently?",
                    text: "Anywhere in the application that references this member\'s id will most likely error. Proceed at your own risk. This can not be un-done.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Continue",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false
                }, function (isConfirmed) {
                    if (isConfirmed) {
                        window.location = $("a[name='delete_member_perm']").attr('href');
                    }
                });
            });
            @end

            @permission('reactivate-members')
                $("a[name='restore_member']").click(function (e) {
                e.preventDefault();

                swal({
                    title: "Are you Sure to restore this member?",
                    text: "Restore this member to its original state?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Continue",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false
                }, function (isConfirmed) {
                    if (isConfirmed) {
                        window.location = $("a[name='restore_member']").attr('href');
                    }
                });
            });
            @end
        });
    </script>
@stop

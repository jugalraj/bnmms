{!! csrf_field() !!}

<!-- Full Name Form Text Input -->
<div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
    {!! Form::label('full_name','Full Name ',['class' =>'col-md-4 control-label']) !!}

    <div class="col-md-6">
        {!! Form::text('full_name',null,['class'=>'form-control']) !!}

        @if ($errors->has('full_name'))
            <span class="help-block">
                <strong>{{ $errors->first('full_name') }}</strong>
            </span>
        @endif
    </div>
</div>

<!-- Username Form Text Input -->
<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
    {!! Form::label('username','Username ',['class' =>'col-md-4 control-label']) !!}

    <div class="col-md-6">
        {!! Form::text('username',null,['class'=>'form-control', 'disabled']) !!}

        @if ($errors->has('username'))
            <span class="help-block">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif
    </div>
</div>

<!-- Email Address Form Email Input -->
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">E-Mail Address</label>

    <div class="col-md-6">
        {!! Form::input('email','email',null,['class'=>'form-control','required']) !!}

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-btn fa-user"></i>Update
        </button>
    </div>
</div>

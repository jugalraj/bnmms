@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.dashboard.event.header') }}
        <small>{{ trans('labels.dashboard.event.view') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/fullcalendar.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <div id="calendar">{!! $calendar->calendar() !!}</div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class ="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Upcomming Events</h3>
                </div>
                <div class="box-body">
                    <ul class="list-unstyled list-header">
                        @foreach($calendar_events as $calendar_event)
                            @if(($calendar_event->end >= \Carbon\Carbon::now()) and $calendar_event->disabled_status == 1)
                                <li><a href="#"><h5> {{$calendar_event->title}}</h5></a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class ="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Completed Events</h3>
                </div>
                <div class="box-body">
                    <ul class="list-unstyled list-header">
                        @foreach($calendar_events as $calendar_event)
                            @if(($calendar_event->end <= \Carbon\Carbon::now()) and $calendar_event->disabled_status == 1)
                                <li><a href="#"><h5> {{$calendar_event->title}}</h5></a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class ="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Canceled Events</h3>
                </div>
                <div class="box-body">
                    <ul class="list-unstyled list-header">
                        @foreach($calendar_events as $calendar_event)
                            @if($calendar_event->disabled_status == 0)
                                <li><a href="#"><h5> {{$calendar_event->title}}</h5></a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
@endsection

@section('after-scripts-end')
    {!! Html::script('assets/js/moment.min.js') !!}
    {!! Html::script('assets/js/fullcalendar.min.js') !!}
    {!! $calendar->script() !!}
@endsection
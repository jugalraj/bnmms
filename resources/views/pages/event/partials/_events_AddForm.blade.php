<div class="row">
    <div class="col-md-8">

        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title','Title ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('title','title',$event->title,['class'=>'form-control']) !!}

                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('desc','Event Description ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::textarea('desc',$event->desc,['class'=>'form-control', 'rows' => '3']) !!}

                @if ($errors->has('desc'))
                    <span class="help-block">
        <strong>{{ $errors->first('desc') }}</strong>
        </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
            {!! Form::label('start','Start Date ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('start','start',$event->start,['class'=>'form-control']) !!}

                @if ($errors->has('start'))
                    <span class="help-block">
                        <strong>{{ $errors->first('start') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('end') ? ' has-error' : '' }}">
            {!! Form::label('end','End Date ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('end','end',$event->end,['class'=>'form-control']) !!}

                @if ($errors->has('organization'))
                    <span class="help-block">
                        <strong>{{ $errors->first('organization') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('background_color') ? ' has-error' : '' }}">
            {!! Form::label('background_color','Background Color',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('background_color','background_color',$event->background_color,['class'=>'form-control']) !!}

                @if ($errors->has('background_color'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mobile') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="pull-right col-xs-10 ">
                <button type="reset" class="btn btn-primary">
                    <i class="fa fa-btn fa-sign-in"></i> Reset
                </button>
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-sign-in"></i> {{ $submitText }}
                </button>
            </div>
        </div>
    </div>
</div>
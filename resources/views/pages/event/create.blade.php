@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.dashboard.event.header') }}
        <small>{{ trans('labels.dashboard.event.create') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.css') }}">
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.dashboard.event.create') }}</h3>
        </div>
        <div class="box-body">
            {!! Form::open(['route' => 'dashboard.events.store', 'class' => 'form-horizontal', 'id' => 'form-wizard' , 'files'=>'true']) !!}
                @include('pages.event.partials._events_AddForm')
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('after-scripts-end')
    {!! Html::script('assets/js/moment.min.js') !!}
    {!! Html::script('assets/js/datepicker.js') !!}
    <script type="text/javascript">
            $('#start').datepicker({
                timepicker: true
            });
            $('#end').datepicker({
                timepicker: true
            });
    </script>
@endsection
@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.dashboard.event.index') }}
        <small>{{ trans('labels.dashboard.event.canceled') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
    <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="eventsTable" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>All Day Event</th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach($calendar_events as $calendar_event)
                                    <tr>
                                        <td>{{$calendar_event->id}}</td>
                                        <td>{{$calendar_event->title}}</td>
                                        <td>{{$calendar_event->start}}</td>
                                        <td>{{$calendar_event->end}}</td>
                                        <td>{{$calendar_event->is_all_day}}</td>
                                        <td class="text-right">
                                            <div class="action-buttons pull-right">{!! $calendar_event->action_buttons !!}</div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                            <a class="btn btn-success" href="{{ route('dashboard.events.create') }}">Create</a>
                        </div>
                    </div>
                    <div id="calendar"></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->
        </div>
    </div>
@endsection

@section('after-scripts-end')
    {!! Html::script('assets/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/js/dataTables.bootstrap.min.js') !!}
    <script>
        $(function () {
            $('#eventsTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": false,
                "autoWidth": false
            });
        });
    </script>
@endsection
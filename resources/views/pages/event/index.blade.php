@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.dashboard.event.index') }}
        <small>{{ trans('labels.dashboard.event.view') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
    <!-- /.col -->
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title">
                <h4>Upcoming Events</h4>
            </div>
            @permission('create-alliances')
            <div class="box-tools pull-right">
                <a class="btn btn-success" href="{{ route('dashboard.events.create') }}">
                    <i class="fa fa-calendar-plus-o"></i> Add
                </a>
            </div>
            @end
        </div>
        <div class="box-body">
            <table id="eventsTable" class="table table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Event Type</th>
                    <th>All Day Event</th>
                </tr>
                </thead>

                <tbody>

                @foreach($calendar_events as $calendar_event)
                    @if($calendar_event->end >= \Carbon\Carbon::now())
                        <tr>
                            <td>{{$calendar_event->title}}</td>
                            <td>{{$calendar_event->start}}</td>
                            <td>{{$calendar_event->end}}</td>
                            <td></td>
                            <td>{{$calendar_event->is_all_day}}</td>
                            <td class="text-right">
                                <div class="action-buttons pull-right">{!! $calendar_event->action_buttons !!}</div>
                            </td>
                        </tr>
                    @endif
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <div class="box-title">
                <h4>Completed Events</h4>
            </div>
        </div>
        <div class="box-body">
            <table id="eventsTable" class="table table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Started Date</th>
                    <th>Completed Date</th>
                    <th>Event Type</th>
                    <th>All Day Event</th>
                </tr>
                </thead>

                <tbody>

                @foreach($calendar_events as $calendar_event)
                    @if($calendar_event->end <= \Carbon\Carbon::now())
                        <tr>
                            <td>{{$calendar_event->title}}</td>
                            <td>{{$calendar_event->start}}</td>
                            <td>{{$calendar_event->end}}</td>
                            <td></td>
                            <td>{{$calendar_event->is_all_day}}</td>
                            <td class="text-right">
                                <div class="action-buttons pull-right">{!! $calendar_event->action_buttons !!}</div>
                            </td>
                        </tr>
                    @endif
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('after-scripts-end')
    {!! Html::script('assets/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/js/dataTables.bootstrap.min.js') !!}
    <script>
        $(function () {
            $('#eventsTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": false,
                "autoWidth": false
            });
        });
    </script>
@endsection
@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.emails.header') }}
        <small></small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title">

            </div>
            <div class="box-tools pull-right">
                <a class="btn btn-primary" href="{{ route('email.create') }}">
                    <i class="fa fa-pencil-square"></i> {{ trans('labels.admin.emails.compose') }}
                </a>
            </div>
        </div>
        <div class="box-body no-padding">
            <table id="mailboxTable" class="table table-hover table-striped">
                <thead>
                <tr>
                    <td></td>
                    <td>Flag</td>
                    <td>Composer</td>
                    <td>Attachment</td>
                    <td>Description</td>
                    <td>Time</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><input type="checkbox"></td>
                    <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a>
                    </td>
                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                    <td class="mailbox-subject"><b>AdminLTE 2.0 Issue</b> - Trying to find a
                        solution to
                        this problem...
                    </td>
                    <td class="mailbox-attachment"></td>
                    <td class="mailbox-date">5 mins ago</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('after-scripts-end')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#mailboxTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": false,
                "autoWidth": false
            });
        });
    </script>
@endsection
@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.members.payment.header') }}
        <small>{{ trans('labels.admin.members.payment.edit') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.admin.members.payment.edit') }}</h3>
        </div>

        <div class="box-body">
            {{--{{ dd($member) }}--}}
            {!! Form::model($payment, ['method' => 'PUT','route' => ['dashboard.member.payment.update',$member->id,$payment->id], 'class' => 'form-horizontal']) !!}
                @include('pages.payments._edit_payment')
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection


@section('after-scripts-end')
    <script>
        $(document).ready(function () {
            toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
            //this will call our toggleFields function every time the selection value of our underAge field changes
            $("#payment_modes").change(function () {
                toggleFields();
            });

        });
        //this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
        function toggleFields() {
            if ($("#payment_modes").val() == 'Cheque') {
                $("#cheque_number").prop('required', true);
                $("#Cheque").show();
            }
            else {
                $("#cheque_number").prop('required', false);
                $("#Cheque").hide();
            }
        }
    </script>
@endsection
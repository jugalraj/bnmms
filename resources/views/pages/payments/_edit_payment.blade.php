<div class="row">
    <div class="col-md-6">

        <div class="form-group{{ $errors->has('member_name') ? ' has-error' : '' }}">
            {!! Form::label('member_name','Member Name ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">
                {!! Form::input('text','member_name',$member->getFullName(),['class'=>'form-control', 'disabled']) !!}

                @if ($errors->has('member_name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('member_name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <!-- Hidden Value for Member ID Form Text Input -->
        {!! Form::input('hidden','member_id',$member->id,['class'=>'form-control']) !!}

                <!-- Payment Date Form Text Input -->
        <div class="form-group{{ $errors->has('payment_date') ? ' has-error' : '' }}">
            {!! Form::label('payment_date','Payment Date ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">
                {!! Form::input('date','payment_date',date('Y-m-d'),['class'=>'form-control']) !!}

                @if ($errors->has('payment_date'))
                    <span class="help-block">
                    <strong>{{ $errors->first('payment_date') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <!-- Receipt Id Form Text Input -->
        <div class="form-group{{ $errors->has('receipt_id') ? ' has-error' : '' }}">
            {!! Form::label('receipt_id','Receipt Id ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">
                {!! Form::input('number','receipt_id',null,['class'=>'form-control']) !!}

                @if ($errors->has('receipt_id'))
                    <span class="help-block">
                    <strong>{{ $errors->first('receipt_id') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <!-- Payment Modes Form Text Input -->
        <div class="form-group{{ $errors->has('payment_modes') ? ' has-error' : '' }}">
            {!! Form::label('payment_modes','Payment Modes ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">

                {{ Form::select('payment_modes', ['Cash' => 'Cash', 'Bank Transfer/Deposit' => 'Bank Transfer/Deposit', 'Remittance' => 'Remittance', 'E-sewa' => 'E-sewa', 'nPAY' => 'nPAY'], $payment->payment_modes, ['class' => 'form-control']) }}

                @if ($errors->has('payment_modes'))
                    <span class="help-block">
                    <strong>{{ $errors->first('payment_modes') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <!-- Payment Amount Form Text Input -->
        <div class="form-group{{ $errors->has('payment_amount') ? ' has-error' : '' }}">
            {!! Form::label('payment_amount','Payment Amount (Rs.) ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">
                {!! Form::input('number','payment_amount',null,['class'=>'form-control', 'step'=>"0.01"]) !!}

                @if ($errors->has('payment_amount'))
                    <span class="help-block">
                    <strong>{{ $errors->first('payment_amount') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <!-- Payment Purpose Form Text Input -->
        <div class="form-group{{ $errors->has('purpose') ? ' has-error' : '' }}">
            {!! Form::label('purpose','Purpose ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">

                {{ Form::select('purpose', ['Membership Fee' => 'Membership Fee', 'Operational Donation' => 'Operational Donation', 'Campaigns Donation' => 'Campaigns Donation', 'Election Donation' => 'Election Donation', 'Relief Fund' => 'Relief Fund', 'Others' => 'Others'], $payment->purpose, ['class' => 'form-control']) }}

                @if ($errors->has('purpose'))
                    <span class="help-block">
                    <strong>{{ $errors->first('purpose') }}</strong>
                </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
                <!-- Comments Form Text Input -->
        <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }}">
            {!! Form::label('comments','Comments ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::textarea('comments',null,['class'=>'form-control', 'rows' => '5']) !!}

                @if ($errors->has('comments'))
                    <span class="help-block">
                    <strong>{{ $errors->first('comments') }}</strong>
                </span>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="pull-right col-xs-3 ">
        <button type="reset" class="btn btn-primary">
            <i class="fa fa-btn fa-sign-in"></i> {{ trans('labels.general.buttons.reset') }}
        </button>
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-btn fa-sign-in"></i>{{ trans('labels.general.buttons.update') }}
        </button>
    </div>
</div>

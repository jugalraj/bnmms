<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all' => 'All',
        'yes' => 'Yes',
        'no' => 'No',
        'custom' => 'Custom',
        'actions' => 'Actions',
        'buttons' => [
            'save' => 'Save',
            'update' => 'Update',
            'reset' => 'Reset'
        ],
        'hide' => 'Hide',
        'none' => 'None',
        'show' => 'Show',
        'toggle_navigation' => 'Toggle Navigation',
    ],

    'admin' => [
        'acl' => [
            'permissions' => [
                'label' => 'permissions',
                'header' => 'Permission Management',
                'create' => 'Create Permission',
                'edit' => 'Edit Permission',
                'delete' => 'Delete Permission',
                'dependencies' => 'Dependencies',

                'grouped_permissions' => 'Grouped Permissions',
                'ungrouped_permissions' => 'Ungrouped Permissions',
                'no_groups' => 'No Groups available in database.',
                'no_permissions' => 'No permissions available in database.',
                'no_roles' => 'No Roles available in database',
                'no_ungrouped' => 'No Ungrouped permission available',

                'table' => [
                    'dependencies' => 'Dependencies',
                    'group' => 'Group',
                    'group-sort' => 'Group Sort',
                    'name' => 'Name',
                    'permission' => 'Permission',
                    'roles' => 'Roles',
                    'system' => 'System',
                    'total' => 'permission total|permissions total',
                    'users' => 'Users',
                ],

                'tabs' => [
                    'general' => 'General',
                    'groups' => 'All Groups',
                    'dependencies' => 'Dependencies',
                    'permissions' => 'All Permissions',
                ],
            ],

            'groups' => [
                'create' => 'Create Group',
                'edit' => 'Edit Group',
                'delete' => 'Delete Group',

                'table' => [
                    'name' => 'Name',
                ],
            ],

            'roles' => [
                'create' => 'Create Role',
                'edit' => 'Edit Role',
                'delete' => 'Delete Role',
                'header' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions' => 'Permissions',
                    'role' => 'Role',
                    'sort' => 'Sort',
                    'total' => 'role total|roles total',
                ],
            ],

            'users' => [
                'create' => 'Create User',
                'edit' => 'Edit User',
                'delete' => 'Delete User',
                'active' => 'Active Users',
                'deactivated' => 'Deactivated Users',
                'deleted' => 'Deleted Users',
                'header' => 'User Management',
                'change_password' => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'permissions' => 'Permissions',
                'dependencies' => 'Dependencies',
                'all_permissions' => 'All Permissions',
                'no_other_permissions' => 'No Other Permissions',
                'no_permissions' => 'No Permissions available',
                'no_roles' => 'No Roles available.',
                'no_deleted_users' => 'No Deleted Users',
                'no_deactivated_users' => 'No Deactivated Users',
                'permission_check' => 'Checking a permission will also check its dependencies, if any.',

                'table' => [
                    'confirmed' => 'Confirmed',
                    'created' => 'Created',
                    'email' => 'E-mail',
                    'id' => 'ID',
                    'last_updated' => 'Last Updated',
                    'name' => 'Name',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted' => 'No Deleted Users',
                    'other_permissions' => 'Other Permissions',
                    'roles' => 'Roles',
                    'total' => 'user total|users total',
                    'actions' => 'Actions'],
            ],
        ],

        'setting' => [
            'header' => 'Settings',
            'config' => 'Website Configuration',
        ],

        'guest' => [

            'auth' => [
                'login_box_title' => 'Login',
                'login_button' => 'Login',
                'register_box_title' => 'Register',
                'register_button' => 'Register',
                'remember_me' => 'Remember Me',
            ],

            'passwords' => [
                'forgot_password' => 'Forgot Your Password?',
                'reset_password_box_title' => 'Reset Password',
                'reset_password_button' => 'Reset Password',
                'send_password_reset_link' => 'Send Password Reset Link',
                'change' => 'Change Password',
            ],

            'profile' => [
                'avatar' => 'Avatar',
                'created_at' => 'Created At',
                'edit_information' => 'Edit Information',
                'email' => 'E-mail',
                'last_updated' => 'Last Updated',
                'name' => 'Name',
                'update_info' => 'Update Information',
            ],
        ],

        'peoples' => [
            'label' => 'Peoples',
            'header' => 'Peoples Management',
            'create' => 'Add People',
            'edit' => 'Edit People',
            'delete' => 'Delete People',
            'deleted' => 'Deleted People',
            'view' => 'View People',
            'active' => 'Active People',
            'disabled' => 'Disabled People',
            'expired' => 'Expired People',
            'show' => 'Show People Details',
            'total' => 'people total|peoples total',
            'payment' => [
                'header' => 'People\'s Payment',
                'create' => 'New Payment',
                'add' => 'Make Payment of ',
                'edit' => 'Edit Payment',
                'delete' => 'Delete Payment',
                'view' => 'Browse all Payment'
            ]
        ],

        'members' => [
            'label' => 'Members',
            'header' => 'Members Management',
            'create' => 'Add Member',
            'edit' => 'Edit Member',
            'delete' => 'Delete Member',
            'deleted' => 'Deleted Member',
            'view' => 'View Members',
            'active' => 'Active Members',
            'disabled' => 'Disabled Member',
            'expired' => 'Membership Expired Members',
            'show' => 'Show Member Details',
            'total' => 'member total|members total',
            'payment' => [
                'header' => 'Member\'s Payment',
                'create' => 'New Payment',
                'add' => 'Make A Payment ',
                'edit' => 'Edit Payment',
                'delete' => 'Delete Payment',
                'view' => 'Browse all Payment'
            ]
        ],

        'events' => [
            'label' => 'Events',
            'index' => 'Event Management',
            'header' => 'Event Management',
            'create' => 'Create Event',
            'edit' => 'Edit Event',
            'delete' => 'Delete Event',
            'view' => 'View Events',
            'cancel' => 'Cancel Event',
            'canceled' => 'Canceled Events',
            'deleted' => 'Deleted Events',
            'calendar' => 'Calendar',
            'upcoming' => 'Upcoming Events',
        ],

        'alliances' => [
            'label' => 'Alliances',
            'index' => 'Alliance Management',
            'header' => 'Alliance Management',
            'active' => 'Active Alliance',
            'create' => 'Add Alliance',
            'edit' => 'Edit Alliance',
            'delete' => 'Delete Alliance',
            'view' => 'View Alliance',
            'total' => 'Alliance in Total|Alliances in Total',
        ],

        'emails' => [
            'label' => 'Mailbox',
            'index' => 'Alliance Management',
            'header' => 'MailBox',
            'mails' => 'Mails',
            'scheduled' => 'Scheduled',
            'all' => 'All Mails',
            'delete' => 'Delete Mail',
            'view' => 'View Mail',
            'compose' => 'Compose',
        ]
    ],
];

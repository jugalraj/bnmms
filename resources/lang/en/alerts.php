<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'dashboard' => [
        'permissions' => [
            'created' => 'Permission successfully created.',
            'deleted' => 'Permission successfully deleted.',

            'groups' => [
                'created' => 'Permission group successfully created.',
                'deleted' => 'Permission group successfully deleted.',
                'updated' => 'Permission group successfully updated.',
            ],

            'updated' => 'Permission successfully updated.',
        ],

        'roles' => [
            'created' => 'The role has been successfully created.',
            'deleted' => 'The role has been successfully deleted.',
            'updated' => 'The role has been successfully updated.',
        ],

        'users' => [
            'confirmation_email' => 'A new confirmation e-mail has been sent to the address on file.',
            'created' => 'The user has been successfully created.',
            'deleted' => 'The user has been successfully deleted.',
            'deleted_permanently' => 'The user has been deleted permanently.',
            'restored' => 'The user has been successfully restored.',
            'updated' => 'The user has been successfully updated.',
            'updated_password' => "The user's password has been successfully updated.",
        ],

        'members' => [
            'created' => 'New Member has been added successfully.',
            'updated' => 'Members Information has been updated successfully.'
        ]
    ],
];
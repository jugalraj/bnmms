<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'dashboard' => [
        'access' => [
            'users' => [
                'activate' => 'Activate',
                'change_password' => 'Change Password',
                'deactivate' => 'Deactivate',
                'delete_permanently' => 'Delete Permanently',
                'resend_email' => 'Resend Confirmation E-mail',
                'restore_user' => 'Restore User',
            ],
        ],
        'members' => [
            'activate' => 'Activate Member',
            'deactivate' => 'Deactivate member',
            'delete_permanently' => 'Delete Permanently',
        ],
        'events' => [
            'activate' => 'Activate Event',
            'deactivate' => 'Cancel Event',
            'delete_permanently' => 'Delete Event',
        ]
    ],

    'general' => [
        'cancel' => 'Cancel',

        'crud' => [
            'create' => 'Create',
            'delete' => 'Delete',
            'edit' => 'Edit',
            'update' => 'Update',
        ],
        'make_member'   => 'Upgrade to member',
        'save' => 'Save',
        'view' => 'View',
        'payment' => 'Payments'
    ],
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'admin' => [
        'acl' => [
            'title' => 'Access Management',

            'permissions' => [
                'all' => 'All Permissions',
                'create' => 'Create Permission',
                'edit' => 'Edit Permission',
                'groups' => [
                    'all' => 'All Groups',
                    'create' => 'Create Group',
                    'edit' => 'Edit Group',
                    'main' => 'Groups',
                ],
                'main' => 'Permissions',
                'management' => 'Permission Management',
            ],

            'roles' => [
                'all' => 'All Roles',
                'create' => 'Create Role',
                'edit' => 'Edit Role',
                'management' => 'Role Management',
                'main' => 'Roles',
            ],

            'users' => [
                'all' => 'All Users',
                'change-password' => 'Change Password',
                'create' => 'Create User',
                'deactivated' => 'Deactivated Users',
                'deleted' => 'Deleted Users',
                'edit' => 'Edit User',
                'main' => 'Users',
            ],
        ],

        'setting'   =>  [
            'main'      =>  'Settings',
            'config'    =>  'Website Configuration',
        ],

        'log-viewer' => [
            'main' => 'Log Viewer',
            'dashboard' => 'Dashboard',
            'logs' => 'Logs',
        ],

        'sidebar' => [
            'dashboard' => 'Dashboard',
            'general' => 'General',
        ],

        'people' => [
            'main' => 'Peoples',
            'add' => 'Add People',
            'view' => 'View Peoples',
        ],

        'member' => [
            'main' => 'Members',
            'add' => 'Add Member',
            'view' => 'View Members',
            'deleted' => 'Deleted Members',
            'inactive' => 'Membership Expired Members',
            'disabled' => 'Disabled Members',
        ],

        'alliances' => [
            'main' => 'Alliances',
            'add' => 'Add Alliance',
            'view' => 'View Alliances',
            'deleted' => 'Deleted Alliances',
            'group' => 'Alliances Group'
        ],

        'events' => [
            'main' => 'Events',
            'add' => 'Create Event',
            'view' => 'View Events',
            'calendar' => 'View Calendar',
            'canceled' => 'Canceled Events',
            'deleted' => 'Deleted Events',
        ]
    ],
];
